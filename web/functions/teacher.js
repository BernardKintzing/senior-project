/**
 * Hold functions called by the teacher
 */
const functions = require("firebase-functions")
const admin = require("firebase-admin")
const firestore = admin.firestore()
const objects = require('./objects');


// send request from teacher to student
exports.request_student = functions.https.onCall((data, context) => {
    const teacher_id = data.teacher_id
    const emails = data.emails
    const user_ref = firestore.collection("accounts")

    var retrieve_user_promises = []
    var update_user_promises = []

    // async return from function will hold result for each email
    var result = Object.assign({}, objects.async_return)

    emails.forEach((email) => {
        retrieve_user_promises.push(user_ref.where("email", "==", email).get())
    })

    return Promise.all(retrieve_user_promises)
        .then((snapshots) => {
            snapshots.forEach((snapshot) => {
                if (!snapshot.empty) {
                    // There should only be a single doc in the snapshot if there
                    // is more that one, we messed up allowing duplicate emails
                    snapshot.forEach((doc) => {

                        update_user_promises.push(
                            firestore
                                .collection("accounts")
                                .doc(doc.id)
                                .update({
                                    teacher_requests: admin.firestore.FieldValue.arrayUnion(teacher_id),
                                })
                        )
                    })
                }
            })

            return Promise.all(update_user_promises)
                .then(() => {
                    result.success = true
                    result.error = null
                    return result
                })
                .catch((error) => {
                    result.success = false
                    result.error = error
                    return result
                })
        })
        .catch((error) => {
            result.success = false
            result.error = error
            return result
        })
})

// approve students request to join teacher
exports.approve_student = functions.https.onCall((data, context) => {
    const teacher_id = data.teacher_id
    const student_id = data.student_id
    const user_ref = firestore.collection("accounts")

    var promises = []

    var result = Object.assign({}, objects.async_return)

    // remove request from students account
    promises.push(
        user_ref.doc(teacher_id).update({
            student_requests: admin.firestore.FieldValue.arrayRemove(student_id),
        })
    )

    // add teacher account to student profile
    promises.push(
        user_ref.doc(student_id).update({
            current_teachers: admin.firestore.FieldValue.arrayUnion(teacher_id),
        })
    )

    // add student account to teacher profile
    promises.push(
        user_ref.doc(teacher_id).update({
            current_students: admin.firestore.FieldValue.arrayUnion(student_id),
        })
    )

    return Promise.all(promises)
        .then(() => {
            result.success = true
            return result
        })
        .catch((error) => {
            result.success = false
            result.error = error
            return result
        })
})

// deny students request to join teacher
exports.deny_student = functions.https.onCall((data, context) => {
    const teacher_id = data.teacher_id
    const student_id = data.student_id

    var result = Object.assign({}, objects.async_return)

    // remove request from students account
    return firestore
        .collection("accounts")
        .doc(teacher_id)
        .update({
            student_requests: admin.firestore.FieldValue.arrayRemove(student_id),
        })
        .then(() => {
            result.success = true
            return result
        })
        .catch((error) => {
            result.success = false
            result.error = error
            return result
        })
})

