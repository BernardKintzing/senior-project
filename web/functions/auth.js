/**
 * Hold functions related to user identification, profile, and permissions
 */
const functions = require("firebase-functions")
const admin = require("firebase-admin")
const firestore = admin.firestore()
const objects = require("./objects")

// return the name of user with a provided uid
// TODO: consider security of who can access function, potentially refactoring to profile_for_uid()
exports.name_for_uid = functions.https.onCall((data, context) => {
    const uid = data.uid

    var result = Object.assign({}, objects.async_return)

    return firestore
        .collection("accounts")
        .doc(uid)
        .get()
        .then((doc) => {
            if (doc.exists) {
                const data = doc.data()

                const preferred_name = data.preferred_name
                const first_name = data.first_name

                result.success = true

                if (preferred_name != null && preferred_name != "") {
                    result.return = preferred_name
                } else if (first_name != null || first_name != "") {
                    result.return = first_name
                } else {
                    // teacher has no name field saved in their profile
                    result.return = "Unknown User"
                }

                return result
            }
        })
        .catch((error) => {
            result.success = false
            result.error = error
            return result
        })
})

// TODO: dangerous to pass out uid
exports.uid_for_email = functions.https.onCall((data, context) => {
    const email = data.email

    var result = Object.assign({}, objects.async_return)

    return firestore
        .collection("accounts")
        .where("email", "==", email)
        .get()
        .then((snapshot) => {
            // There should only be a single doc in the snapshot if there
            // is more that one, we messed up allowing duplicate emails
            snapshot.forEach((doc) => {
                result.success = true
                result.return = {id: doc.id, account_type: doc.data().account_type}
            })

            return result
        })
        .catch((error) => {
            result.success = false
            result.error = error
            return result
        })
})
