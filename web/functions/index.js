const admin = require("firebase-admin")
admin.initializeApp({
    credential: admin.credential.applicationDefault(),
})

exports.auth = require('./auth');
exports.student = require('./student');
exports.teacher = require('./teacher');
exports.staff = require('./staff');
