/**
 * Hold functions called by staff
 */
 const functions = require("firebase-functions")
 const admin = require("firebase-admin")
 const firestore = admin.firestore()
 const objects = require("./objects")

exports.submit_passes = functions.https.onCall((data, context) => {
    const students = data.students
    const date = data.date
    const origin = data.origin
    const destination = data.destination
    const teachers = data.teachers
    const reason = data.reason
    const school = data.school
    const pass_ref = firestore.collection("passes")

    var result = Object.assign({}, objects.async_return)
    var promises = []

    students.forEach((student_id) => {
        promises.push(
            pass_ref
                .add({
                    student_id,
                    date: admin.firestore.Timestamp.fromDate(new Date(date)),
                    origin,
                    destination,
                    teachers,
                    reason,
                    school,
                })
        )
    })

    return Promise.all(promises)
        .then(() => {
            result.success = true
            return result
        })
        .catch((error) => {
            result.success = false
            result.error = error
            return result
        })
})
