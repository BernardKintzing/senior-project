$(() => {

    var $active_item = undefined,
        $content = $("#content")
        
    var active_page_name = undefined

    // load a new content page based on selected item
    window.load_page = function ($item) {

        var page_name = $item.attr("page_name")

        $content = $("#content")

        $.get(`/assets/components/${current_user.profile.account_type}/pages/${page_name}.html`, function (component) {
            $.getScript(`/assets/js/${current_user.profile.account_type}/pages/${page_name}.js`)

            // disable old item
            $("#menu").find(".item").each(function (index, menu_item) {
                menu_item.classList.remove("active")
            })

            // remove old js file
            if (active_page_name != undefined) {
                $("head").find(`script[src="/assets/js/${current_user.profile.account_type}/pages/${active_page_name}.js]`).remove()
            }

            // make new item active
            $(`.item[page_name=${page_name}]`).addClass("active")
            $content.html(component)

            // update params
            var url = new URL(window.location.href)
            url.searchParams.set("page", page_name)

            var doc_title = "Hall Pass | " + page_name.charAt(0).toUpperCase() + page_name.slice(1);

            document.title = doc_title

            window.history.pushState({ html: component, pageTitle: "Hall Pass | " + doc_title }, "", url)

            $active_item = $item
            active_page_name = page_name
        })
    }

    // listen for tabs click
    $("body").on("click", ".tabs button", function () {

        $name = $(this).attr('name')
        $content = $('.tab[name="' + $name + '-content"]')

        $(this).closest(".tabs").find("button").each(function (index) {
            $(this).removeClass("active")
        });

        $content.closest(".content").find(".tab").each(function (index) {
            $(this).hide()
        });

        $(this).addClass("active")
        $content.show()
    })

    // listen for menu item clicks
    $("body").on("click", ".item", function() {
        window.load_page($(this))
    })

    // listen for modal close click
    $("body").on("click", ".modal .close", function () {
        $(this).closest(".modal").hide()
    })
});

function getParams(url) {
    var params = {}
    var parser = document.createElement("a")
    parser.href = url
    var query = parser.search.substring(1)
    var vars = query.split("&")
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split("=")
        params[pair[0]] = decodeURIComponent(pair[1])
    }
    return params
}