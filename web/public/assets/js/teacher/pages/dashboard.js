$(() => {

    var new_student_emails = []

    // functions to detatch listeners
    var pass_request_unsub = undefined
    attachListeners()

    // update UI if profile is updated
    current_user.listener.register(() => {
        attachListeners()
        refresh()
    })

    window.students.listener.register(() => {
        refresh()
    })

    refresh()

    // add click listeners
    $("#view-all-upcoming-passes-button").click(function () { window.load_page($(this)) })

    $("#view-all-past-passes-button").click(function () { window.load_page($(this)) })

    $("#add-students-button").click(() => {
        $("#add-students-modal").show()

        if (new_student_emails.length == 0) {
            $("#send-new-student-invite").hide()
        } else {
            $("#send-new-student-invite").show()
        }
    })

    $("#view-students-button").click(function () { window.load_page($(this)) })

    $("#add-student-email-button").click(() => {
        $("#add-student-email-field")
            .val()
            .split(",")
            .forEach((email) => {
                email = $.trim(email)

                if (!new_student_emails.includes(email) && email != "") {
                    $("#new-email-list").append(`<div class="email"><p>` + email + `</p><div class="fa-times remove-email"></div></div>`)
                    new_student_emails.push(email)
                }

                if (new_student_emails.length == 0) {
                    $("#send-new-student-invite").hide()
                } else {
                    $("#send-new-student-invite").show()
                }
            })
    })

    $("#send-new-student-invite").click(() => {
        if (current_user.auth != null && current_user.profile != null) {
            var call = functions.httpsCallable("teacher-request_student")
            call({
                teacher_id: current_user.auth.uid,
                emails: new_student_emails,
            })
                .then((result) => {
                    result = result["data"]
                    if (result.success == true) {
                        alert("Requests successfully sent.")
                    } else {
                        if (result.error != null) {
                            alert(result.error)
                            console.log(result.error)
                        } else {
                            alert("Error: An unknown error occured when attempting to submit requests, please try again later.")
                        }
                    }
                })
                .catch(function (error) {
                    alert(error)
                    console.log(error)
                })
        } else {
            alert("Error: Unable to retrieve account, please try again later.")
        }
    })

    $("body").on("click", ".remove-email", function () {
        var $component = $(this).closest(".email")
        $component.remove()

        var $email = $component.find("p").first().text()
        var $email_index = new_student_emails.indexOf($email)
        if ($email_index != -1) {
            new_student_emails.splice($email_index, 1)

            if (new_student_emails.length == 0) {
                $("#send-new-student-invite").hide()
            } else {
                $("#send-new-student-invite").show()
            }
        }
    })

    // accept a students request
    $("body").on("click", "[action=accept_student_request]", function () {
        if (current_user.auth != null && current_user.profile != null) {
            var s_uid = $(this).closest("[s_uid]").attr("s_uid")

            var call = functions.httpsCallable("teacher-approve_student")
            call({
                teacher_id: current_user.auth.uid,
                student_id: s_uid,
            })
                .then((result) => {
                    result = result["data"]
                    if (result.success == false) {
                        if (result.error != null) {
                            alert(result.error)
                        } else {
                            alert("Error: An unknown error occured when attempting to submit requests, please try again later.")
                        }
                    }
                })
                .catch(function (error) {
                    alert(error)
                })
        } else {
            alert("Error: Unable to retrieve account, please try again later.")
        }
    })

    // reject a students request
    $("body").on("click", "[action=reject_student_request]", function () {
        if (current_user.auth != null && current_user.profile != null) {
            var s_uid = $(this).closest("[s_uid]").attr("s_uid")

            var call = functions.httpsCallable("teacher-deny_student")
            call({
                teacher_id: current_user.auth.uid,
                student_id: s_uid,
            })
                .then((result) => {
                    result = result["data"]

                    if (result.success == false) {
                        if (result.error != null) {
                            alert(result.error)
                        } else {
                            alert("Error: An unknown error occured when attempting to reject the request, please try again later.")
                        }
                    }
                })
                .catch(function (error) {
                    alert(error)
                })
        } else {
            alert("Error: Unable to retrieve account, please try again later.")
        }
    })

    // accept pass request
    $("body").on("click", "[action=accept_pass_request]", function () {
        var p_id = $(this).closest("[p_id]").attr("p_id")

        firestore
            .collection("passes")
            .doc(p_id)
            .update({
                is_approved: true,
            })
            .catch((error) => {
                console.error(error)
            })
    })

    // reject pass request
    $("body").on("click", "[action=reject_pass_request]", function () {
        var p_id = $(this).closest("[p_id]").attr("p_id")

        firestore
            .collection("passes")
            .doc(p_id)
            .update({
                is_approved: false,
            })
            .catch((error) => {
                console.error(error)
            })
    })

    function attachListeners() {
        // if there is an active listener we detach first
        if (pass_request_unsub != undefined) {
            pass_request_unsub()
        }

        pass_request_unsub = firestore
            .collection("passes")
            .where("teachers", "array-contains", current_user.auth.uid)
            .where("date", ">=", new Date())
            .orderBy("date", "desc")
            .limit(5)
            .onSnapshot(
                (snapshots) => {
                    var $up_content = $("#upcoming-passes")
                    var $up_t_body = $up_content.find("tbody")

                    $.get("/assets/components/teacher/elements/upcoming-pass-row.html", function (up_component) {
                        // reset table body content
                        $up_content.hide()
                        $up_t_body.empty()

                        snapshots.forEach((doc) => {
                            var data = doc.data()

                            var $up_object = $($.parseHTML(up_component))
                            $up_object.attr("p_id", doc.id)

                            var date = data.date.toDate()
                            var date_string = `${date.getMonth() + 1}/${date.getDate()}/${date.getFullYear()} ${date.getHours()}:${date.getMinutes()}`
                            $up_object.find("td[row_data=time]").find("p").text(date_string)

                            // update students name
                            if (data.student_id in window.students_id_dict) {
                                $up_object.find("td[row_data=student]").find("p").text(window.students_id_dict[data.student_id])
                            } else {
                                $up_object.find("td[row_data=student]").find("p").text("Unknown Student")
                            }

                            $up_object.find("td[row_data=reason]").find("p").text(data.reason)

                            var is_approved = data.is_approved
                            if (is_approved == undefined) {
                                // pass is pending approval
                                $up_object.find("td[row_data=accept_pass]").find("button").removeClass("selected")
                                $up_object.find("td[row_data=reject_pass]").find("button").removeClass("selected")
                            } else if (is_approved == false) {
                                // pass was denied
                                $up_object.find("td[row_data=accept_pass]").find("button").removeClass("selected")
                                $up_object.find("td[row_data=reject_pass]").find("button").addClass("selected")
                            } else {
                                // pass was approved
                                $up_object.find("td[row_data=accept_pass]").find("button").addClass("selected")
                                $up_object.find("td[row_data=reject_pass]").find("button").removeClass("selected")
                            }

                            $up_t_body.append($up_object)

                            $up_content.show()
                        })
                    })
                },
                (error) => {
                    console.error(error)
                }
            )
    }

    function refresh() {
        displayStudentRequests()
        displayStudents()
        displayPastPasses()
    }

    // display active student requests
    function displayStudentRequests() {
        var student_requests = current_user.profile.student_requests
        var $sr_content = $("#student-requests")
        var $sr_t_body = $sr_content.find("tbody")

        // reset table body content
        $sr_content.hide()
        $sr_t_body.empty()

        if (student_requests != null && student_requests.length > 0) {
            $.get("/assets/components/teacher/elements/student-request-row.html", function (sr_component) {

                // reset table body content
                $sr_content.hide()
                $sr_t_body.empty()

                student_requests.forEach((student_id) => {
                    var $sr_object = $($.parseHTML(sr_component))

                    $sr_object.attr("s_uid", student_id)

                    // query students name
                    console.log($sr_object.find(".title"))
                    if (student_id in window.students_id_dict) {
                        window.students_id_dict[student_id]
                        $sr_object.find(".title").text(window.students_id_dict[student_id])
                    } else {
                        $sr_object.find(".title").text("Unknown Student")
                    }

                    $sr_t_body.append($sr_object)
                })

                $sr_content.show()
            })
        }
    }

    // display current student
    function displayStudents() {
        var $cs_content = $("#current-students"),
            $cs_t_body = $cs_content.find("tbody")
        $student_list = $("#student-list")

        // reset table body content
        $cs_t_body.empty()
        $cs_content.hide()

        $.get("/assets/components/teacher/elements/student-row.html", function (cs_component) {
            // reset table body content
            $cs_t_body.empty()
            $cs_content.hide()
            
            window.students.array.forEach((student) => {
                var name = "Unknown Student"

                if (student.preferred_name != undefined && student.preferred_name != "") {
                    name = student.preferred_name
                } else if (student.first_name != undefined && student.first_name != "") {
                    name = student.first_name
                }

                if (student.linked) {
                    var $cs_object = $($.parseHTML(cs_component))
                    $cs_object.find(".title").text(name)
                    $cs_object.find(".email").text(student.email)
                    $cs_object.find(".more-info").hide()
                    $cs_object.attr("t_uid", student.id)

                    $cs_t_body.append($cs_object)
                }

                // while we are looping through the teachers we can populate the teacher list
                $student_list.append(`<option value="${student.email}" label="${name}">`)
            })

            $cs_content.show()
        })
    }

    function displayPastPasses() {
        firestore
            .collection("passes")
            .where("teachers", "array-contains", current_user.auth.uid)
            .where("date", "<", new Date())
            .orderBy("date", "desc")
            .limit(5)
            .get()
            .then((snapshots) => {
                var $pp_content = $("#past-passes")
                var $pp_t_body = $pp_content.find("tbody")
                $.get("/assets/components/teacher/elements/past-pass-row.html", function (pp_component) {

                    // reset table body content
                    $pp_content.hide()
                    $pp_t_body.empty()

                    snapshots.forEach((doc) => {
                        var data = doc.data()

                        var $pp_object = $($.parseHTML(pp_component))
                        $pp_object.attr("p_id", doc.id)

                        var date = data.date.toDate()
                        var date_string = `${date.getMonth() + 1}/${date.getDate()}/${date.getFullYear()} ${date.getHours()}:${date.getMinutes()}`
                        $pp_object.find("td[row_data=time]").find("p").text(date_string)

                        // query students name
                        if (data.student_id in window.students_id_dict) {
                            $pp_object.find("td[row_data=student]").find("p").text(window.students_id_dict[data.student_id])
                        } else {
                            $pp_object.find("td[row_data=student]").find("p").text("Unknown Student")
                        }

                        $pp_object.find("td[row_data=destination]").find("p").text(data.destination)

                        $pp_t_body.append($pp_object)

                        $pp_content.show()
                    })
                })
            })
            .catch((error) => {
                console.error(error)
            })
    }
})
