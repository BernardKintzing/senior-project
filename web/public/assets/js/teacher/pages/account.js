$(() => {

    var $schools_input = $("#schools-input"),
        $school_name = $("#school-name"),
        $school_email = $("#school-email"),
        $school_phone = $("#school-phone"),
        $school_address = $("#school-address"),
        $join_school_button = $("#join-school")
    refresh()

    // update UI if profile is updated
    current_user.listener.register(() => {
        refresh()
    })

    $("#update-profile").click(() => {
        var selected_image = document.getElementById("select-image").files[0]
        var first_name = $("#first_name").val()
        var last_name = $("#last_name").val()
        var preferred_name = $("#preferred_name").val()

        if (current_user.auth != null) {
            if (selected_image != undefined) {
                storage
                    .ref(`profile_images/${current_user.auth.uid}`)
                    .put(selected_image)
                    .then(function (snapshot) {
                        snapshot.ref.getDownloadURL().then((url) => {
                            current_user.auth
                                .updateProfile({
                                    photoURL: url,
                                })
                                .then(() => {
                                    $("#profile-image").attr("src", url)
                                })
                                .catch((error) => {
                                    console.error(error)
                                })
                        })
                    })
                    .catch((error) => {
                        console.error(error)
                    })
            }

            if (first_name != undefined && last_name != undefined) {
                firestore
                    .collection("accounts")
                    .doc(current_user.auth.uid)
                    .update({
                        first_name,
                        last_name,
                        preferred_name,
                    })
                    .catch((error) => {
                        console.error(error)
                    })
            }
        }
    })

    $schools_input.change(() => {
        var val = ($schools_input).val()

        firestore
            .collection("schools")
            .doc(val)
            .get()
            .then((doc) => {

                const data = doc.data()

                var address = ""
                if (data.address_line_one) { address += `${data.address_line_one}, ` }
                if (data.address_line_two) { address += `${data.address_line_two}, ` }
                if (data.city) { address += `${data.city}, ` }
                if (data.state) { address += `${data.city} ` }
                if (data.postal_code) { address += `${data.postal_code} ` }

                $school_name.html(data.name)
                $school_email.html(data.primary_email)
                $school_phone.html(data.primary_phone)
                $school_address.html(address)

                $join_school_button.data("school-id", doc.id)

                $join_school_button.click(() => {
                    if (current_user.auth) {
                        var school_id = $join_school_button.data("school-id")

                        firestore
                            .collection("accounts")
                            .doc(current_user.auth.uid)
                            .update({
                                school: school_id,
                            })
                            .catch((error) => {
                                alert(error)
                            })
                    }
                })
            })
    })

    $("#sign-out").click(() => {
        auth.signOut()
            .then(() => {
                window.location = "/"
            })
            .catch(function (error) {
                console.error(error)
            })
    })

    function refresh() {
        $("#first_name").val(current_user.profile.first_name)
        $("#last_name").val(current_user.profile.last_name)
        $("#preferred_name").val(current_user.profile.preferred_name)

        if (current_user.auth.photoURL != null) {
            $("#profile-image").attr("src", current_user.auth.photoURL)
        }

        firestore
            .collection("schools")
            .get()
            .then((snapshots) => {

                snapshots.docs.forEach((doc) => {

                    var data = doc.data(),
                        school_id = doc.id

                    $schools_input.append(`<option value="${school_id}">${data.name}</value>`)
                })

                if (current_user.profile.school) {
                    $schools_input.val(current_user.profile.school).trigger('change');
                }
            })
    }
})
