$(() => {
    if (current_user.profile.preferred_name != undefined && current_user.profile.preferred_name != "") {
        $("#header").find("p").first().text(`Welcome ${current_user.profile.preferred_name}`)
    } else if (current_user.profile.first_name != undefined && current_user.profile.first_name != "") {
        $("#header").find("p").first().text(`Welcome ${current_user.profile.first_name}`)
    }

    // variables
    var $menu = $("#menu")
    var params = getParams(window.location.href)

    window.students = {
        array: [],
        listener: {
            listener_functions: [],
            register: function (listener) {
                this.listener_functions.push(listener)
            },
            trigger: function () {
    
                this.listener_functions.forEach((func) => {
                    func()
                })
            },
        },
    }

    window.students_id_dict = {}

    refresh()
    current_user.listener.register(() => {
        refresh()
    })

    // intitialize the content with first item
    if (params["page"] != undefined) {
        window.load_page($menu.find(`.item[page_name=${params["page"]}]`))
    } else {
        window.load_page($menu.find(".item").first())
    }

    window.onpopstate = function(e){
        if(e.state){
            params = getParams(window.location.href)
            window.load_page($menu.find(`.item[page_name=${params["page"]}]`))
        }
    };

    function refresh() {
        if (current_user.profile) {
            var school_id = current_user.profile.school
            if (school_id != undefined && school_id != "") {
                firestore
                    .collection("accounts")
                    .where("school", "==", school_id)
                    .where("account_type", "==", "student")
                    .get()
                    .then((snapshot) => {

                        window.students.array = []

                        snapshot.forEach((doc) => {
                            var student = Object.assign({}, account_data)
                            const data = doc.data()

                            student.uid = doc.id
                            student.first_name = data.first_name
                            student.last_name = data.last_name
                            student.preferred_name = data.preferred_name
                            student.email = data.email

                            if (current_user.profile.current_students != null && current_user.profile.current_students.includes(doc.id)) {
                                student.linked = true
                            }

                            if (student.preferred_name != "") {
                                window.students_id_dict[doc.id] = student.preferred_name
                            } else {
                                window.students_id_dict[doc.id] = student.first_name
                            }

                            window.students.array.push(student)
                        })
                        
                        window.students.listener.trigger()
                    })
                    .catch((error) => {
                        alert(error)
                    })
            }
        }
    }
})
