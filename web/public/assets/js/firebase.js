// firebase variables
var auth = firebase.auth()
var firestore = firebase.firestore()
var functions = firebase.functions()
var storage = firebase.storage()

var current_user = {
    auth: null,
    profile: null,
    listener: {
        listener_functions: [],
        register: function (listener) {
            this.listener_functions.push(listener)
        },
        trigger: function () {

            this.listener_functions.forEach((func) => {
                func()
            })
        },
    },
}

// holds the function to detatch listener from profile
var profile_unsub = undefined

// auth functions

auth.onAuthStateChanged(function (user) {
    if (user) {
        current_user.auth = user
        attachProfileListener()
        current_user.listener.trigger()
    } else {
        current_user.auth = user
        current_user.listener.trigger()
    }
})

// firestore functions

async function createFirestoreAccount(account, account_type) {
    var result = Object.assign({}, async_return)

    return firestore
        .collection("accounts")
        .doc(account.user.uid)
        .set({
            email: account.user.email,
            account_type: account_type,
        })
        .then(() => {
            result.success = true
            return result
        })
        .catch((error) => {
            result.success = false
            result.error = error
            return result
        })
}

function attachProfileListener() {
    // if there is an active listener we detach first
    if (profile_unsub != undefined) {
        profile_unsub()
    }

    profile_unsub = firestore
        .collection("accounts")
        .doc(current_user.auth.uid)
        .onSnapshot(
            (snapshot) => {
                current_user.profile = snapshot.data()
                current_user.listener.trigger()
            },
            (error) => {
                console.error(error)
            }
        )
}