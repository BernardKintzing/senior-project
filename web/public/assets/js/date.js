$(() => {
    // load external date picker files
    // data picker: https://github.com/fengyuanchen/datepicker
    $.getScript("/assets/external/date-picker/datepicker.js", () => {
        $('[data-toggle="datepicker"]').datepicker({
            autoPick: true,
            format: "mm/dd/yyyy",
        })
    })
    $("<link/>", {
        rel: "stylesheet",
        type: "text/css",
        href: "/assets/external/date-picker/datepicker.css",
    }).appendTo("head")

    var date = new Date($.now());

    // populate input fields
    var $hour_input = $("input[current_hour=true]") 
    if ($hour_input != undefined) {
        $hour_input.val(date.getHours() % 12 || 12)
    }

    var $minute_input = $("input[current_minute=true]") 
    if ($minute_input != undefined) {
        $minute_input.val(date.getMinutes())
    }

    var $period_input = $("input[list=period-datalist]")
    if ($period_input != undefined) {
        $period_input.val(date.getHours() < 12 ? "AM" : "PM")
    } 

    // populate period data list
    var $period_datalist = $("#period-datalist")
    if ($period_datalist != undefined) {
        $period_datalist.append($("<option></option>").val("AM").html("AM"))
        $period_datalist.append($("<option></option>").val("PM").html("PM"))
    }
})
