$(() => {
    var $body = $("body")

    current_user.listener.register(() => {
        if (current_user.auth) {
            // user is signed in

            if (current_user.profile) {
                // request appropriate HTML file
                $.get("/assets/components/" + current_user.profile.account_type + "/wrapper.html", function (componenet) {
                    $body.html(componenet)

                    // add appropriate JS file
                    $.getScript("/assets/js/" + current_user.profile.account_type + "/main.js")
                })
            }
        } else {
            // user is signed out

            $.get("/assets/components/index/unsigned_user.html", function (componenet) {
                $body.html(componenet)

                // link elements to functions
                var $sign_in_error_message = $("#sign-in-form-error-message")
                var $sign_up_error_message = $("#sign-up-form-error-message")

                $("#sign-in").click(() => {
                    var $email = $("#sign-in-email").val()
                    var $password = $("#sign-in-password").val()

                    auth.signInWithEmailAndPassword($email, $password).catch(function (error) {
                        $sign_in_error_message.text(error)
                        $sign_in_error_message.show()
                    })
                })

                $("#sign-up").click(() => {
                    var $email = $("#sign-up-email").val()
                    var $password = $("#sign-up-password").val()
                    var $confirm_password = $("#sign-up-confirm-password").val()
                    var $account_type = $("input:radio[name='account-type']:checked").val()

                    if($email == "") {
                        $sign_up_error_message.text("Error: Email is required.")
                        $sign_up_error_message.show()
                        return
                    }

                    if ($password != $confirm_password) {
                        $sign_up_error_message.text("Error: Passwords do not match.")
                        $sign_up_error_message.show()
                        return
                    }

                    if ($account_type == null) {
                        $sign_up_error_message.text("Error: Account type must be selected.")
                        $sign_up_error_message.show()
                        return
                    }

                    auth.createUserWithEmailAndPassword($email, $password)
                        .then(function (account) {
                            account.user.sendEmailVerification().catch(function (error) {
                                $sign_up_error_message.text(error)
                                $sign_up_error_message.show()
                            })

                            createFirestoreAccount(account, $account_type).then(function (result) {
                                switch (result.success) {
                                    case true:
                                        // no action needs to occur if write is succesfull
                                        break
                                    case false:
                                        if (result.error != null) {
                                            $sign_up_error_message.text(error)
                                            $sign_up_error_message.show()
                                        }
                                        break
                                    default:
                                        $sign_up_error_message.text("Error: Unknown action occured when attempting to write new " + $account_type + " account to Firebase Firestore.")
                                        $sign_up_error_message.show()
                                }
                            })
                        })
                        .catch(function (error) {
                            $sign_up_error_message.text(error)
                            $sign_up_error_message.show()
                        })
                })
            })
        }
    })
})
