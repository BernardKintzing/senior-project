$(() => {
    // variables
    var $all_students_table = $("#all-students"),
        $all_students_t_body = $all_students_table.find("tbody"),
        $student_info_modal = $("#student-info-modal")

    reload_table()

    // update UI if profile is updated
    current_user.listener.register(() => {
        reload_table()
    })

    window.students.listener.register(() => {
        reload_table()
    })

    sorttable.makeSortable(document.getElementById("all-students"))

    $("body").on("click", ".student-more-info", function() {

        var student = $(this).data("student")

        $student_info_modal.find("#preferred_name").text(`Preferred Name: ${student.preferred_name}`)
        $student_info_modal.find("#first_name").text(`First Name: ${student.first_name}`)
        $student_info_modal.find("#last_name").text(`Last Name: ${student.last_name}`)
        $student_info_modal.find("#email").text(`Email: ${student.email}`)

        $student_info_modal.show()
    })

    function reload_table() {

        $.get("/assets/components/staff/elements/student-row.html", function (component) {

            window.students.array.forEach((student) => {

                var $object = undefined
                var name = "Unknown Student"

                if (student.preferred_name != undefined && student.preferred_name != "") {
                    name = student.preferred_name
                } else if (student.first_name != undefined && student.first_name != "") {
                    name = student.first_name
                }
                
                $object = $($.parseHTML(component))
                $object.find(".title").text(name)
                $object.find(".email").text(student.email)
                $object.find(".student-more-info").data("student", student)

                $all_students_t_body.append($object)
            })
        })
    }
})
