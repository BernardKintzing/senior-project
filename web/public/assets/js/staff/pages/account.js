$(() => {

    refresh()

    // update UI if profile is updated
    current_user.listener.register(() => {
        refresh()
    })

    $("#update-profile").click(() => {
        var selected_image = document.getElementById("select-image").files[0]
        var first_name = $("#first_name").val()
        var last_name = $("#last_name").val()
        var preferred_name = $("#preferred_name").val()

        if (current_user.auth != null) {
            if (selected_image != undefined) {
                storage
                    .ref(`profile_images/${current_user.auth.uid}`)
                    .put(selected_image)
                    .then(function (snapshot) {
                        snapshot.ref.getDownloadURL().then((url) => {
                            current_user.auth
                                .updateProfile({
                                    photoURL: url,
                                })
                                .then(() => {
                                    $("#profile-image").attr('src', url);
                                })
                                .catch((error) => {
                                    console.error(error)
                                })
                        })
                    })
                    .catch((error) => {
                        console.error(error)
                    })
            }

            if (first_name != undefined && last_name != undefined) {
                firestore
                    .collection("accounts")
                    .doc(current_user.auth.uid)
                    .update({
                        first_name,
                        last_name,
                        preferred_name,
                    })
                    .catch((error) => {
                        console.error(error)
                    })
            }
        }
    })

    $("#sign-out").click(() => {
        auth.signOut()
            .then(() => {
                window.location = "/"
            })
            .catch(function (error) {
                console.error(error)
            })
    })

    function refresh() {
        $("#first_name").val(current_user.profile.first_name)
        $("#last_name").val(current_user.profile.last_name)
        $("#preferred_name").val(current_user.profile.preferred_name)

        if (current_user.auth.photoURL != null) {
            $("#profile-image").attr("src", current_user.auth.photoURL)
        }
    }
})
