$(() => {
    // variables
    var $all_teachers_table = $("#all-teachers-table"),
        $all_teachers_t_body = $all_teachers_table.find("tbody"),
        $teacher_info_modal = $("#teacher-info-modal")

    // update UI if profile is updated
    current_user.listener.register(() => {
        refresh()
    })

    window.teachers.listener.register(() => {
        refresh()
    })

    $("body").on("click", ".teacher-more-info", function() {

        console.log("clicked")
        var teacher = $(this).data("teacher")

        $teacher_info_modal.find("#preferred_name").text(`Preferred Name: ${teacher.preferred_name}`)
        $teacher_info_modal.find("#first_name").text(`First Name: ${teacher.first_name}`)
        $teacher_info_modal.find("#last_name").text(`Last Name: ${teacher.last_name}`)
        $teacher_info_modal.find("#email").text(`Email: ${teacher.email}`)

        $teacher_info_modal.show()
    })

    refresh()

    // to make table sortable, need to pass in vanilla js DOM element
    sorttable.makeSortable(document.getElementById("all-teachers-table"))

    function refresh() {

        $.get("/assets/components/staff/elements/teacher-row.html", function (component) {

            window.teachers.array.forEach((teacher) => {

                var $object = undefined
                var name = "Unknown Teacher"

                if (teacher.preferred_name != undefined && teacher.preferred_name != "") {
                    name = teacher.preferred_name
                } else if (teacher.first_name != undefined && teacher.first_name != "") {
                    name = teacher.first_name
                }

                $object = $($.parseHTML(component))
                $object.find(".title").text(name)
                $object.find(".email").text(teacher.email)
                $object.find(".teacher-more-info").data("teacher", teacher)

                $all_teachers_t_body.append($object)
            })
        })
    }
})
