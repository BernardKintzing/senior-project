$(() => {

    // shared variables
    var $page_content = $("#page-content"),
        $school_name = undefined,
        $primary_email = undefined,
        $primary_phone = undefined,
        $address_line_one = undefined,
        $address_line_two = undefined,
        $city = undefined,
        $state = undefined,
        $postal_code = undefined,
        $country = undefined

    // variable specific to school-page_unassociated
    var $create_school = undefined

    // variable specific to school-page_associated
    var $school_name_header = undefined,
        $update_school = undefined

    var email_regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/,
        phone_regex = /([0-9]{10})|(\([0-9]{3}\)\s+[0-9]{3}\-[0-9]{4})/,
        postal_code_regex = /^\d{5}(?:-\d{4})?$/;

    refresh()

    // update UI if profile is updated
    current_user.listener.register(() => {
        refresh()
    })

    function refresh() {
        if (current_user.profile.school) {
            // load school information

            $.get("/assets/components/staff/school-page_associated.html", (componenet) => {
                $page_content.html(componenet)

                // update elements 
                $school_name_header = $("#school-name-header")
                $update_school = $("#update-school")
                $school_name = $("#school-name")
                $primary_email = $("#primary-email")
                $primary_phone = $("#primary-phone")
                $address_line_one = $("#address-line-one")
                $address_line_two = $("#address-line-two")
                $city = $("#city")
                $state = $("#state")
                $postal_code = $("#postal-code")
                $country = $("#country")

                firestore
                    .collection("schools")
                    .doc(current_user.profile.school)
                    .get()
                    .then((doc) => {
                        const data = doc.data()

                        // populate school information
                        $school_name_header.text(data.name)
                        $school_name.val(data.name)
                        $primary_email.val(data.primary_email)
                        $primary_phone.val(data.primary_phone)
                        $address_line_one.val(data.address_line_one)
                        $address_line_two.val(data.address_line_two)
                        $city.val(data.city)
                        $state.val(data.state)
                        $postal_code.val(data.postal_code)
                        $country.val(data.country)

                    }).catch((error) => {
                        alert(error)
                    })

                $update_school.click(() => {

                    if (validate_form()) {
                        // add new school to databse
                        firestore
                            .collection("schools")
                            .doc(current_user.profile.school)
                            .update({
                                name: $school_name.val(),
                                primary_email: $primary_email.val(),
                                primary_phone: $primary_phone.val(),
                                address_line_one: $address_line_one.val(),
                                address_line_two: $address_line_two.val(),
                                city: $city.val(),
                                state: $state.val(),
                                postal_code: $postal_code.val(),
                                country: $country.val(),
                            })
                            .then(() => {
                                refresh()
                            })
                            .catch((error) => {
                                alert(error)
                            })
                    }

                })
            })
        } else {
            $.get("/assets/components/staff/school-page_unassociated.html", (componenet) => {
                $page_content.html(componenet)

                // update elements 
                $create_school = $("#create-school")
                $school_name = $("#school-name")
                $primary_email = $("#primary-email")
                $primary_phone = $("#primary-phone")
                $address_line_one = $("#address-line-one")
                $address_line_two = $("#address-line-two")
                $city = $("#city")
                $state = $("#state")
                $postal_code = $("#postal-code")
                $country = $("#country")

                var $schools_input = $("#schools-input")

                firestore
                    .collection("schools")
                    .get()
                    .then((snapshots) => {

                        snapshots.docs.forEach((doc) => {

                            var data = doc.data(),
                                school_id = doc.id

                            $schools_input.append(`<option value="${school_id}">${data.name}</value>`)
                        })
                    })

                $create_school.click(() => {

                    if (validate_form()) {
                        // add new school to databse
                        firestore
                            .collection("schools")
                            .add({
                                name: $school_name.val(),
                                primary_email: $primary_email.val(),
                                primary_phone: $primary_phone.val(),
                                address_line_one: $address_line_one.val(),
                                address_line_two: $address_line_two.val(),
                                city: $city.val(),
                                state: $state.val(),
                                postal_code: $postal_code.val(),
                                country: $country.val(),
                            })
                            .then((doc) => {

                                // link account to the school
                                if (current_user.auth) {
                                    firestore
                                        .collection("accounts")
                                        .doc(current_user.auth.uid)
                                        .update({
                                            school: doc.id,
                                        })
                                        .catch((error) => {
                                            alert(error)
                                        })
                                } else {
                                    alert("Error: Unable to automatically link account to school, please try again manually.")
                                }
                            })
                            .catch((error) => {
                                alert(error)
                            })
                    }

                })

                $schools_input.change(() => {
                    var val = ($schools_input).val()

                    firestore
                        .collection("schools")
                        .doc(val)
                        .get()
                        .then((doc) => {

                            const data = doc.data()

                            $.get("/assets/components/staff/elements/join-school-card.html", (card) => {
                                var $object = $($.parseHTML(card)),
                                    $join_school_button = $object.find("#join-school")


                                var address = ""
                                if (data.address_line_one) { address += `${data.address_line_one}, ` }
                                if (data.address_line_two) { address += `${data.address_line_two}, ` }
                                if (data.city) { address += `${data.city}, ` }
                                if (data.state) { address += `${data.city} ` }
                                if (data.postal_code) { address += `${data.postal_code} ` }

                                $object.find("#school-name_card").html(data.name)
                                $object.find("#school-phone_card").html(data.primary_phone)
                                $object.find("#school-email_card").html(data.primary_email)
                                $object.find("#school-address_card").html(address)

                                $join_school_button.data("school-id", doc.id)

                                $("#school-preview").html($object)

                                $join_school_button.click(() => {
                                    if (current_user.auth) {
                                        var school_id = $join_school_button.data("school-id")

                                        firestore
                                            .collection("accounts")
                                            .doc(current_user.auth.uid)
                                            .update({
                                                school: school_id,
                                            })
                                            .catch((error) => {
                                                alert(error)
                                            })
                                    }
                                })
                            })
                        })
                })
            })
        }
    }

    function validate_form() {
        // validate form
        var isValid = true

        if ($school_name.val() == "") {
            $school_name.addClass("invalid")
            isValid = false
        } else {
            $school_name.removeClass("invalid")
        }

        if ($primary_email.val() == "" || !email_regex.test($primary_email.val())) {
            $primary_email.addClass("invalid")
            isValid = false
        } else {
            $primary_email.removeClass("invalid")
        }

        if ($primary_phone.val() == "" || !phone_regex.test($primary_phone.val())) {
            $primary_phone.addClass("invalid")
            isValid = false
        } else {
            $primary_phone.removeClass("invalid")
        }

        if ($address_line_one.val() == "") {
            $address_line_one.addClass("invalid")
            isValid = false
        } else {
            $address_line_one.removeClass("invalid")
        }

        if ($city.val() == "") {
            $city.addClass("invalid")
            isValid = false
        } else {
            $city.removeClass("invalid")
        }

        if ($state.val() == "") {
            $state.addClass("invalid")
            isValid = false
        } else {
            $state.removeClass("invalid")
        }

        if ($postal_code.val() == "" || !postal_code_regex.test($postal_code.val())) {
            $postal_code.addClass("invalid")
            isValid = false
        } else {
            $postal_code.removeClass("invalid")
        }

        if ($country.val() == "") {
            $country.addClass("invalid")
            isValid = false
        } else {
            $country.removeClass("invalid")
        }

        return isValid
    }
})