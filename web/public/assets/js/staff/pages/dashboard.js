$(() => {

    // functions to detatch listeners
    var pass_request_unsub = undefined

    // update UI if profile is updated
    current_user.listener.register(() => {
        refresh()
    })

    attachListeners()
    refresh()

    // add click listeners
    $("#view-all-upcoming-passes-button").click(function () { window.load_page($(this)) })

    $("#view-all-past-passes-button").click(function () { window.load_page($(this)) })

    // accept pass request
    $("body").on("click", "[action=accept_pass_request]", function () {
        var p_id = $(this).closest("[p_id]").attr("p_id")

        firestore
            .collection("passes")
            .doc(p_id)
            .update({
                is_approved: true,
            })
            .catch((error) => {
                console.error(error)
            })
    })

    // reject pass request
    $("body").on("click", "[action=reject_pass_request]", function () {
        var p_id = $(this).closest("[p_id]").attr("p_id")

        firestore
            .collection("passes")
            .doc(p_id)
            .update({
                is_approved: false,
            })
            .catch((error) => {
                console.error(error)
            })
    })

    function attachListeners() {
        // if there is an active listener we detach first
        if (pass_request_unsub != undefined) {
            pass_request_unsub()
        }

        pass_request_unsub = firestore
            .collection("passes")
            .where("school", "==", current_user.profile.school)
            .where("date", ">=", new Date())
            .orderBy("date", "desc")
            .limit(5)
            .onSnapshot(
                (snapshots) => {
                    var $up_content = $("#upcoming-passes")
                    var $up_t_body = $up_content.find("tbody")

                    $.get("/assets/components/staff/elements/upcoming-pass-row.html", function (up_component) {
                        // reset table body content
                        $up_content.hide()
                        $up_t_body.empty()

                        snapshots.forEach((doc) => {
                            var data = doc.data()

                            var $up_object = $($.parseHTML(up_component))
                            $up_object.attr("p_id", doc.id)

                            var date = data.date.toDate()
                            var date_string = `${date.getMonth() + 1}/${date.getDate()}/${date.getFullYear()} ${date.getHours()}:${date.getMinutes()}`
                            $up_object.find("td[row_data=time]").find("p").text(date_string)

                            // query students name

                            if (data.student_id in window.students_id_dict) {
                                $up_object.find("td[row_data=student]").find("p").text(window.students_id_dict[data.student_id])
                            } else {
                                $up_object.find("td[row_data=student]").find("p").text("Unknown Student")
                            }

                            $up_object.find("td[row_data=reason]").find("p").text(data.reason)

                            var is_approved = data.is_approved
                            if (is_approved == undefined) {
                                // pass is pending approval
                                $up_object.find("td[row_data=accept_pass]").find("button").removeClass("selected")
                                $up_object.find("td[row_data=reject_pass]").find("button").removeClass("selected")
                            } else if (is_approved == false) {
                                // pass was denied
                                $up_object.find("td[row_data=accept_pass]").find("button").removeClass("selected")
                                $up_object.find("td[row_data=reject_pass]").find("button").addClass("selected")
                            } else {
                                // pass was approved
                                $up_object.find("td[row_data=accept_pass]").find("button").addClass("selected")
                                $up_object.find("td[row_data=reject_pass]").find("button").removeClass("selected")
                            }

                            $up_t_body.append($up_object)

                            $up_content.show()
                        })
                    })
                },
                (error) => {
                    console.error(error)
                }
            )
    }

    function refresh() {
        displayPastPasses()
    }

    function displayPastPasses() {
        firestore
            .collection("passes")
            .where("school", "==", current_user.profile.school)
            .where("date", "<", new Date())
            .orderBy("date", "desc")
            .limit(5)
            .get()
            .then((snapshots) => {
                var $pp_content = $("#past-passes")
                var $pp_t_body = $pp_content.find("tbody")
                $.get("/assets/components/staff/elements/past-pass-row.html", function (pp_component) {

                    // reset table body content
                    $pp_content.hide()
                    $pp_t_body.empty()

                    snapshots.forEach((doc) => {
                        var data = doc.data()

                        var $pp_object = $($.parseHTML(pp_component))
                        $pp_object.attr("p_id", doc.id)

                        var date = data.date.toDate()
                        var date_string = `${date.getMonth() + 1}/${date.getDate()}/${date.getFullYear()} ${date.getHours()}:${date.getMinutes()}`
                        $pp_object.find("td[row_data=time]").find("p").text(date_string)

                        // query students name

                        if (data.student_id in window.students_id_dict) {
                            $pp_object.find("td[row_data=student]").find("p").text(window.students_id_dict[data.student_id])
                        } else {
                            $pp_object.find("td[row_data=student]").find("p").text("Unknown Student")
                        }

                        $pp_object.find("td[row_data=destination]").find("p").text(data.destination)

                        $pp_t_body.append($pp_object)

                        $pp_content.show()
                    })
                })
            })
            .catch((error) => {
                console.error(error)
            })
    }

})