$(() => {

    // load dependencies
    $.getScript("/assets/js/date.js")

    // variables
    var $table = $("#hall-passes-table"),
        $t_body = $table.find("tbody")

    var teacher_emails = []
    var student_emails = []

    // update UI if profile is updated
    current_user.listener.register(() => {
        refresh()
    })

    window.teachers.listener.register(() => {
        refreshTeachers()
    })

    window.students.listener.register(() => {
        refreshStudents()
    })

    refresh()

    // make hall passes table sortable, need to pass in vanilla js DOM element
    sorttable.makeSortable(document.getElementById("hall-passes-table"))

    // accept pass request
    $("body").on("click", "[action=accept_pass_request]", function () {
        var p_id = $(this).closest("[p_id]").attr("p_id")

        firestore
            .collection("passes")
            .doc(p_id)
            .update({
                is_approved: true,
            })
            .catch((error) => {
                console.error(error)
            })
    })

    // reject pass request
    $("body").on("click", "[action=reject_pass_request]", function () {
        var p_id = $(this).closest("[p_id]").attr("p_id")

        firestore
            .collection("passes")
            .doc(p_id)
            .update({
                is_approved: false,
            })
            .catch((error) => {
                console.error(error)
            })
    })

    $("body").on("click", ".teacher-remove-email", function () {
        var $component = $(this).closest(".email")
        $component.remove()

        var $email = $component.find("p").first().text()
        var $email_index = teacher_emails.indexOf($email)
        if ($email_index != -1) {
            teacher_emails.splice($email_index, 1)

        }
    })

    $("body").on("click", ".student-remove-email", function () {
        var $component = $(this).closest(".email")
        $component.remove()

        var $email = $component.find("p").first().text()
        var $email_index = student_emails.indexOf($email)
        if ($email_index != -1) {
            student_emails.splice($email_index, 1)
        }
    })

    $("#add-teacher-email-button").click(() => {
        $("#pass-teachers-field")
            .val()
            .split(",")
            .forEach((email) => {
                email = $.trim(email)

                if (!teacher_emails.includes(email) && email != "") {
                    $("#teacher-email-list").append(`<div class="email small"><p>` + email + `</p><div class="fa-times teacher-remove-email remove-email"></div></div>`)
                    teacher_emails.push(email)
                }
            })
    })

    $("#add-student-email-button").click(() => {
        $("#pass-students-field")
            .val()
            .split(",")
            .forEach((email) => {
                email = $.trim(email)

                if (!student_emails.includes(email) && email != "") {
                    $("#student-email-list").append(`<div class="email small"><p>` + email + `</p><div class="fa-times student-remove-email remove-email"></div></div>`)
                    student_emails.push(email)
                }
            })
    })

    $(document).on("submit", "form[name=new-pass-request]", () => {
        var $form = $("form[name=new-pass-request]"),
            $error_message = $("#pass-request-form-error-message"),
            date = $form.find("input[name=pass-date]").val(),
            hour = $form.find("input[name=pass-hour]").val(),
            minute = $form.find("input[name=pass-minute]").val(),
            period = $form.find("input[name=pass-period]").val(),
            origin = $form.find("input[name=pass-origin]").val(),
            destination = $form.find("input[name=pass-destination]").val(),
            teachers = $form.find("input[name=pass-teachers").val(),
            students = $form.find("input[name=pass-students").val(),
            reason = $form.find("input[name=pass-reason]").val()

        // validate form: a majority of the form does not need to be manually validated
        // as the default validation can handle our use case

        // validate period
        if (period != "AM" && period != "PM") {
            $error_message.text("Error: Period must either be AM or PM.")
            $error_message.show()
            return false
        }

        if (current_user.auth != null && current_user.profile != null) {
            // construct date
            var complete_date = `${date} ${hour}:${minute} ${period}`

            var uid_call = functions.httpsCallable("auth-uid_for_email")
            var promises = []

            teacher_emails.forEach((email) => {
                promises.push(uid_call({ email: email }))
            })
            student_emails.forEach((email) => {
                promises.push(uid_call({ email: email }))
            })

            Promise.all(promises)
                .then((id_results) => {
                    teacher_ids = []
                    student_ids = []
                    id_results.forEach((id_result) => {
                        id_result = id_result["data"]
                        if (id_result.success == true) {
                            if (!teacher_ids.includes(id_result.return.id) && !student_ids.includes(id_result.return.id) && id_result.return.id != undefined && id_result.return.account_type != undefined) {

                                if (id_result.return.account_type == "student") {
                                    student_ids.push(id_result.return.id)
                                } else if (id_result.return.account_type == "teacher") {
                                    teacher_ids.push(id_result.return.id)
                                }
                            }
                        }
                    })

                    var call = functions.httpsCallable("staff-submit_passes")
                    call({
                        students: student_ids,
                        date: complete_date,
                        origin: origin,
                        destination: destination,
                        teachers: teacher_ids,
                        reason: reason,
                        school: current_user.profile.school
                    })
                        .then((result) => {
                            result = result["data"]
                            if (result.success == true) {
                                alert("Requests successfully sent.")
                                teacher_emails = []
                                student_emails = []
                            } else {
                                if (result.error != null) {
                                    alert(result.error)
                                } else {
                                    alert("Error: An unknown error occured when attempting to submit requests, please try again later.")
                                }
                            }
                        })
                        .catch(function (error) {
                            console.error(error)
                            alert(error)
                        })
                })
                .catch((error) => {
                    console.error(error)
                    alert(error)
                })
        } else {
            alert("Error: Unable to retrieve account, please try again later.")
        }

        // returning false prevents page reload
        return false
    })

    // listen to updates on passes
    firestore
        .collection("passes")
        .where("school", '==', current_user.profile.school)
        .onSnapshot(
            (snapshots) => {

                // reset table content
                $t_body.empty()

                $.get("/assets/components/staff/elements/hall-passes-row.html", function (component) {
                    $t_body.empty()
                    snapshots.forEach((doc) => {
                        var data = doc.data()

                        var $object = $($.parseHTML(component))
                        $object.attr("p_id", doc.id)

                        var date = data.date.toDate()
                        var date_string = `${date.getMonth() + 1}/${date.getDate()}/${date.getFullYear()} ${date.getHours()}:${(date.getMinutes() < 10 ? "0" : "") + date.getMinutes()}`
                        $object.find("td[row_data=date]").find("p").text(date_string)
                        $object.find("td[row_data=date]").attr("sorttable_customkey", `${date.getFullYear()}${(date.getMonth() < 10 ? "0" : "") + date.getMonth()}${(date.getDate() < 10 ? "0" : "") + date.getDate()}${(date.getHours() < 10 ? "0" : "") + date.getHours()}${(date.getMinutes() < 10 ? "0" : "") + date.getMinutes()}${(date.getSeconds() < 10 ? "0" : "") + date.getSeconds()}`)

                        // query students name
                        if (data.student_id in window.students_id_dict) {
                            $object.find("td[row_data=student]").find("p").text(window.students_id_dict[data.student_id])
                        } else {
                            $object.find("td[row_data=student]").find("p").text("Unknown Student")
                        }

                        $object.find("td[row_data=destination]").find("p").text(data.destination)
                        $object.find("td[row_data=reason]").find("p").text(data.reason)

                        var is_approved = data.is_approved
                        if (is_approved == undefined) {
                            // pass is pending approval
                            $object.find("td[row_data=approved]").attr("sorttable_customkey", 0)
                        } else if (is_approved == false) {
                            // pass was denied
                            $object.find("td[row_data=accept_pass]").find("button").removeClass("selected")
                            $object.find("td[row_data=reject_pass]").find("button").addClass("selected")
                            $object.find("td[row_data=approved]").attr("sorttable_customkey", -1)
                        } else {
                            // pass was approved
                            $object.find("td[row_data=accept_pass]").find("button").addClass("selected")
                            $object.find("td[row_data=reject_pass]").find("button").removeClass("selected")
                            $object.find("td[row_data=approved]").attr("sorttable_customkey", 1)
                        }

                        $t_body.append($object)
                    })
                })
            },
            (error) => {
                console.error(error)
            }
        )

    function refresh() {
        refreshTeachers()
        refreshStudents()
    }

    function refreshTeachers() {
        var $teacher_list = $("#known-teachers")
        window.teachers.array.forEach((teacher) => {
            var name = "Unknown Teacher"

            if (teacher.preferred_name != undefined && teacher.preferred_name != "") {
                name = teacher.preferred_name
            } else if (teacher.first_name != undefined && teacher.first_name != "") {
                name = teacher.first_name
            }

            $teacher_list.append(`<option value="${teacher.email}" label="${name}">`)
        })
    }

    function refreshStudents() {
        var $student_list = $("#known-students")
        window.students.array.forEach((student) => {
            var name = "Unknown Student"

            if (student.preferred_name != undefined && student.preferred_name != "") {
                name = student.preferred_name
            } else if (student.first_name != undefined && student.first_name != "") {
                name = student.first_name
            }

            $student_list.append(`<option value="${student.email}" label="${name}">`)
        })
    }
})