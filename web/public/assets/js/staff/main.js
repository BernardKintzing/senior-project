$(() => {
    if (current_user.profile.preferred_name != undefined && current_user.profile.preferred_name != "") {
        $("#header").find("p").first().text(`Welcome ${current_user.profile.preferred_name}`)
    } else if (current_user.profile.first_name != undefined && current_user.profile.first_name != "") {
        $("#header").find("p").first().text(`Welcome ${current_user.profile.first_name}`)
    }

    // variables
    var $menu = $("#menu")
    var params = getParams(window.location.href)

    window.students = {
        array: [],
        listener: {
            listener_functions: [],
            register: function (listener) {
                this.listener_functions.push(listener)
            },
            trigger: function () {
    
                this.listener_functions.forEach((func) => {
                    func()
                })
            },
        },
    }

    window.students_id_dict = {}

    window.teachers = {
        array: [],
        listener: {
            listener_function: function () { },
            register: function (listener) {
                this.listener_function = listener
            },
            trigger: function () {
                this.listener_function()
            },
        },
    }

    window.teachers_id_dict = {}

    window.staff = {
        array: [],
        listener: {
            listener_function: function () { },
            register: function (listener) {
                this.listener_function = listener
            },
            trigger: function () {
                this.listener_function()
            },
        },
    }

    refresh()
    current_user.listener.register(() => {
        refresh()
    })


    // intitialize the content with first item
    if (params["page"] != undefined) {
        window.load_page($menu.find(`.item[page_name=${params["page"]}]`))
    } else {
        window.load_page($menu.find(".item").first())
    }

    window.onpopstate = function (e) {
        if (e.state) {
            params = getParams(window.location.href)
            window.load_page($menu.find(`.item[page_name=${params["page"]}]`))
        }
    };

    function refresh() {
        if (current_user.profile) {
            var school_id = current_user.profile.school
            if (school_id != undefined && school_id != "") {
                firestore
                    .collection("accounts")
                    .where("school", "==", school_id)
                    .get()
                    .then((snapshot) => {

                        window.students.array = []
                        window.teachers.array = []

                        snapshot.forEach((doc) => {
                            var account = Object.assign({}, account_data)
                            const data = doc.data()

                            account.uid = doc.id
                            account.first_name = data.first_name
                            account.last_name = data.last_name
                            account.preferred_name = data.preferred_name
                            account.email = data.email

                            if (data.account_type == "student") {
                                window.students.array.push(account)

                                if (account.preferred_name != "") {
                                    window.students_id_dict[doc.id] = account.preferred_name
                                } else {
                                    window.students_id_dict[doc.id] = account.first_name
                                }
                            } else if (data.account_type == "teacher") {
                                window.teachers.array.push(account)

                                if (account.preferred_name != "") {
                                    window.teachers_id_dict[doc.id] = account.preferred_name
                                } else {
                                    window.teachers_id_dict[doc.id] = account.first_name
                                }
                            } else if (data.account_type == "staff") {
                                window.staff.array.push(account)
                            }

                        })

                        window.students.listener.trigger()
                        window.teachers.listener.trigger()
                        window.staff.listener.trigger()
                    })
                    .catch((error) => {
                        alert(error)
                    })
            }
        }
    }
})
