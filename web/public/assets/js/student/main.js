$(() => {


    if (current_user.profile.preferred_name != undefined && current_user.profile.preferred_name != "") {
        $("#header").find("p").first().text(`Welcome ${current_user.profile.preferred_name}`)
    } else if (current_user.profile.first_name != undefined && current_user.profile.first_name != "") {
        $("#header").find("p").first().text(`Welcome ${current_user.profile.first_name}`)
    }

    // variables
    var $menu = $("#menu")

    window.teachers = {
        array: [],
        listener: {
            listener_functions: [],
            register: function (listener) {
                this.listener_functions.push(listener)
            },
            trigger: function () {
    
                this.listener_functions.forEach((func) => {
                    func()
                })
            },
        },
    }

    window.teachers_id_dict = {}

    var params = getParams(window.location.href)

    refresh()
    current_user.listener.register(() => {
        refresh()
    })

    // intitialize the content with first item
    if (params["page"] != undefined) {
        window.load_page($menu.find(`.item[page_name=${params["page"]}]`))
    } else {
        window.load_page($menu.find(".item").first())
    }

    window.onpopstate = function (e) {
        if (e.state) {
            params = getParams(window.location.href)
            window.load_page($menu.find(`.item[page_name=${params["page"]}]`))
        }
    }

    function refresh() {
        if (current_user.profile) {
            var school_id = current_user.profile.school
            if (school_id != undefined && school_id != "") {
                firestore
                    .collection("accounts")
                    .where("school", "==", school_id)
                    .where("account_type", "==", "teacher")
                    .get()
                    .then((snapshot) => {

                        window.teachers.array = []

                        snapshot.forEach((doc) => {
                            var teacher = Object.assign({}, account_data)
                            const data = doc.data()

                            teacher.uid = doc.id
                            teacher.first_name = data.first_name
                            teacher.last_name = data.last_name
                            teacher.preferred_name = data.preferred_name
                            teacher.email = data.email

                            if (current_user.profile.current_teachers != null && current_user.profile.current_teachers.includes(doc.id)) {
                                teacher.linked = true
                            }

                            if (teacher.preferred_name != "") {
                                window.teachers_id_dict[doc.id] = teacher.preferred_name
                            } else {
                                window.teachers_id_dict[doc.id] = teacher.first_name
                            }

                            window.teachers.array.push(teacher)
                        })

                        window.teachers.listener.trigger()
                    })
                    .catch((error) => {
                        alert(error)
                    })
            }
        }
    }
})
