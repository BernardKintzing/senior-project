$(() => {

    // load dependencies
    $.getScript("/assets/js/date.js")

    refresh()

    var new_teacher_emails = []
    var pass_teacher_emails = []

    // update UI if profile is updated
    current_user.listener.register(() => {
        refresh()
    })

    window.teachers.listener.register(() => {
        refresh()
    })

    // to make table sortable, need to pass in vanilla js DOM element
    sorttable.makeSortable(document.getElementById("current-passes-table"))

    $("body").on("click", ".remove-new-email", function () {
        var $component = $(this).closest(".email")
        $component.remove()

        var $email = $component.find("p").first().text()
        var $email_index = new_teacher_emails.indexOf($email)
        if ($email_index != -1) {
            new_teacher_emails.splice($email_index, 1)

            if (new_teacher_emails.length == 0) {
                $("#send-new-student-invite").hide()
            } else {
                $("#send-new-student-invite").show()
            }
        }
    })

    $("body").on("click", ".remove-pass-email", function () {
        var $component = $(this).closest(".email")
        $component.remove()

        var $email = $component.find("p").first().text()
        var $email_index = pass_teacher_emails.indexOf($email)
        if ($email_index != -1) {
            pass_teacher_emails.splice($email_index, 1)

            if (pass_teacher_emails.length == 0) {
                $("#send-new-student-invite").hide()
            } else {
                $("#send-new-student-invite").show()
            }
        }
    })

    $("#request-pass-button").click(() => {
        $("#request-pass-modal").show()
    })

    $("#add-teachers-button").click(() => {
        $("#add-teachers-modal").show()

        if (new_teacher_emails.length == 0) {
            $("#send-new-teacher-request").hide()
        } else {
            $("#send-new-teacher-request").show()
        }
    })

    $("#add-teacher-email-button").click(() => {
        $("#add-teacher-email-field")
            .val()
            .split(",")
            .forEach((email) => {
                email = $.trim(email)

                if (!new_teacher_emails.includes(email) && email != "") {
                    $("#new-email-list").append(`<div class="email"><p>` + email + `</p><div class="fa-times remove-email remove-new-email"></div></div>`)
                    new_teacher_emails.push(email)
                }

                if (new_teacher_emails.length == 0) {
                    $("#send-new-teacher-request").hide()
                } else {
                    $("#send-new-teacher-request").show()
                }
            })
    })

    $("#add-teacher-to-pass-email-button").click(() => {
        $("#pass-teachers-field")
            .val()
            .split(",")
            .forEach((email) => {
                email = $.trim(email)

                if (!pass_teacher_emails.includes(email) && email != "") {
                    $("#pass-email-list").append(`<div class="email"><p>` + email + `</p><div class="fa-times remove-email remove-pass-email"></div></div>`)
                    pass_teacher_emails.push(email)
                }
            })
    })

    $("#send-new-teacher-request").click(() => {
        if (current_user.auth != null && current_user.profile != null) {
            var call = functions.httpsCallable("student-request_teacher")
            call({
                student_id: current_user.auth.uid,
                emails: new_teacher_emails,
            })
                .then((result) => {
                    result = result["data"]
                    if (result.success == true) {
                        alert("Requests successfully sent.")
                        new_teacher_emails = []
                    } else {
                        if (result.error != null) {
                            alert(result.error)
                        } else {
                            alert("Error: An unknown error occured when attempting to submit requests, please try again later.")
                        }
                    }
                })
                .catch(function (error) {
                    alert(error)
                })
        } else {
            alert("Error: Unable to retrieve account, please try again later.")
        }
    })

    // accept a teachers request
    $("body").on("click", ".accept", function () {
        if (current_user.auth != null && current_user.profile != null) {
            var t_uid = $(this).closest("[t_uid]").attr("t_uid")

            var call = functions.httpsCallable("student-approve_teacher")
            call({
                student_id: current_user.auth.uid,
                teacher_id: t_uid,
            })
                .then((result) => {
                    result = result["data"]
                    if (result.success == false) {
                        if (result.error != null) {
                            alert(result.error)
                        } else {
                            alert("Error: An unknown error occured when attempting to submit requests, please try again later.")
                        }
                    }
                })
                .catch(function (error) {
                    alert(error)
                })
        } else {
            alert("Error: Unable to retrieve account, please try again later.")
        }
    })

    // reject a teachers request
    $("body").on("click", ".reject", function () {
        if (current_user.auth != null && current_user.profile != null) {
            var t_uid = $(this).closest("[t_uid]").attr("t_uid")

            var call = functions.httpsCallable("student-deny_teacher")
            call({
                student_id: current_user.auth.uid,
                teacher_id: t_uid,
            })
                .then((result) => {
                    result = result["data"]

                    if (result.success == false) {
                        if (result.error != null) {
                            alert(result.error)
                        } else {
                            alert("Error: An unknown error occured when attempting to reject the request, please try again later.")
                        }
                    }
                })
                .catch(function (error) {
                    alert(error)
                })
        } else {
            alert("Error: Unable to retrieve account, please try again later.")
        }
    })

    $(document).on("submit", "form[name=new-pass-request]", () => {
        var $form = $("form[name=new-pass-request]"),
            $error_message = $("#pass-request-form-error-message"),
            date = $form.find("input[name=pass-date]").val(),
            hour = $form.find("input[name=pass-hour]").val(),
            minute = $form.find("input[name=pass-minute]").val(),
            period = $form.find("input[name=pass-period]").val(),
            origin = $form.find("input[name=pass-origin]").val(),
            destination = $form.find("input[name=pass-destination]").val(),
            teachers = $form.find("input[name=pass-teachers").val(),
            reason = $form.find("input[name=pass-reason]").val()

        // validate form: a majority of the form does not need to be manually validated
        // as the default validation can handle our use case

        // validate period
        if (period != "AM" && period != "PM") {
            $error_message.text("Error: Period must either be AM or PM.")
            $error_message.show()
            return false
        }

        if (current_user.auth != null && current_user.profile != null) {
            // construct date
            var complete_date = `${date} ${hour}:${minute} ${period}`

            var uid_call = functions.httpsCallable("auth-uid_for_email")
            var promises = []
            pass_teacher_emails.forEach((email) => {
                promises.push(uid_call({ email: email }))
            })

            Promise.all(promises)
                .then((id_results) => {
                    teacher_ids = []
                    id_results.forEach((id_result) => {
                        id_result = id_result["data"]
                        if (id_result.success == true) {
                            if (!teacher_ids.includes(id_result.return.id) && id_result.return.id != undefined) {
                                teacher_ids.push(id_result.return.id)
                            }
                        }
                    })
                    var call = functions.httpsCallable("student-request_pass")
                    call({
                        student_id: current_user.auth.uid,
                        date: complete_date,
                        origin: origin,
                        destination: destination,
                        teachers: teacher_ids,
                        reason: reason,
                        school: current_user.profile.school
                    })
                        .then((result) => {
                            result = result["data"]
                            if (result.success == true) {
                                displayPasses()
                                alert("Requests successfully sent.")
                                pass_teacher_emails = []
                            } else {
                                if (result.error != null) {
                                    console.error(error)
                                    alert(result.error)
                                } else {
                                    alert("Error: An unknown error occured when attempting to submit requests, please try again later.")
                                }
                            }
                        })
                        .catch(function (error) {
                            console.error(error)
                            alert(error)
                        })
                })
                .catch((error) => {
                    console.error(error)
                    alert(error)
                })
        } else {
            alert("Error: Unable to retrieve account, please try again later.")
        }

        // returning false prevents page reload
        return false
    })

    function refresh() {
        displayTeacherRequests()
        displayTeachers()
        displayPasses()
    }

    // display active teacher requests
    function displayTeacherRequests() {
        var teacher_requests = current_user.profile.teacher_requests
        var $tr_content = $("#teacher-requests")
        var $tr_t_body = $tr_content.find("tbody")

        // reset table body content
        $tr_content.hide()
        $tr_t_body.empty()

        if (teacher_requests != null && teacher_requests.length > 0) {
            $.get("/assets/components/student/elements/teacher-request-row.html", function (tr_component) {

                // reset table body content
                $tr_content.hide()
                $tr_t_body.empty()
                teacher_requests.forEach((teacher_id) => {
                    var $tr_object = $($.parseHTML(tr_component))

                    $tr_object.attr("t_uid", teacher_id)

                    // query teachers name
                    if (teacher_id in window.teachers_id_dict) {
                        $tr_object.find(".title").text(window.teachers_id_dict[teacher_id])
                    } else {
                        $tr_object.find(".title").text("Unknown Teacher")
                    }
                    
                    $tr_t_body.append($tr_object)
                })

                $tr_content.show()
            })
        }
    }

    // display current teachers
    function displayTeachers() {
        var $ct_content = $("#current-teachers"),
            $ct_t_body = $ct_content.find("tbody"),
            $teacher_list = $("#teacher-list"),
            $known_teachers = $("#known-teachers")

        // reset table body content
        $ct_t_body.empty()
        $ct_content.hide()

        $.get("/assets/components/student/elements/current-teacher-row.html", function (ct_component) {

            // reset table body content
            $ct_t_body.empty()
            $ct_content.hide()

            window.teachers.array.forEach((teacher) => {
                var name = "Unknown Teacher"

                if (teacher.preferred_name != undefined && teacher.preferred_name != "") {
                    name = teacher.preferred_name
                } else if (teacher.first_name != undefined && teacher.first_name != "") {
                    name = teacher.first_name
                }

                if (teacher.linked) {
                    var $ct_object = $($.parseHTML(ct_component))
                    $ct_object.find(".title").text(name)
                    $ct_object.attr("t_uid", teacher.id)

                    $ct_t_body.append($ct_object)
                }

                // while we are looping through the teachers we can populate the teacher list
                $teacher_list.append(`<option value="${teacher.email}" label="${name}">`)
                $known_teachers.append(`<option value="${teacher.email}" label="${name}">`)
            })

            $ct_content.show()
        })

    }

    // display current passes
    function displayPasses() {
        if (current_user.auth.uid != undefined) {
            firestore
                .collection("passes")
                .where("student_id", "==", current_user.auth.uid)
                .orderBy("date", "desc")
                .get()
                .then((snapshots) => {
                    var $cp_content = $("#current-passes")
                    var $cp_t_body = $cp_content.find("tbody")

                    // reset table body content
                    $cp_content.hide()
                    $cp_t_body.empty()

                    $.get("/assets/components/student/elements/current-pass-row.html", function (cp_component) {
                        $cp_content.hide()
                        $cp_t_body.empty()
                        snapshots.forEach((doc) => {
                            var data = doc.data()

                            var $cp_object = $($.parseHTML(cp_component))
                            $cp_object.attr("p_id", doc.id)

                            var date = data.date.toDate()
                            var date_string = `${date.getMonth() + 1}/${date.getDate()}/${date.getFullYear()} ${date.getHours()}:${(date.getMinutes() < 10 ? "0" : "") + date.getMinutes()}`
                            $cp_object.find("td[row_data=time]").find("p").text(date_string)

                            $cp_object.find("td[row_data=destination]").find("p").text(data.destination)
                            $cp_object.find("td[row_data=reason]").find("p").text(data.reason)

                            var is_approved = data.is_approved
                            if (is_approved == undefined) {
                                // pass is pending approval
                                $cp_object.find("td[row_data=approved]").find("span").addClass("fa-clock")
                            } else if (is_approved == false) {
                                // pass was denied
                                $cp_object.find("td[row_data=approved]").find("span").addClass("fa-times")
                            } else {
                                // pass was approved
                                $cp_object.find("td[row_data=approved]").find("span").addClass("fa-check ")
                            }
                            $cp_t_body.append($cp_object)

                            $cp_content.show()
                        })
                    })
                })
                .catch((error) => {
                    console.error(error)
                })
        }
    }
})