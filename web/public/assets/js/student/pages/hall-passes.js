$(() => {
    // variables
    var $table = $("#hall-passes-table"),
        $t_body = $table.find("tbody")

    // make hall passes table sortable, need to pass in vanilla js DOM element
    sorttable.makeSortable(document.getElementById("hall-passes-table"))

    // listen to updates on passes
    firestore
        .collection("passes")
        .where("student_id", "==", current_user.auth.uid)
        .onSnapshot(
            (snapshots) => {
                // reset table content
                $t_body.empty()

                $.get("/assets/components/student/elements/hall-passes-row.html", function (component) {
                    snapshots.forEach((doc) => {
                        var data = doc.data()

                        var $object = $($.parseHTML(component))
                        $object.attr("p_id", doc.id)

                        var date = data.date.toDate()
                        var date_string = `${date.getMonth() + 1}/${date.getDate()}/${date.getFullYear()} ${date.getHours()}:${(date.getMinutes() < 10 ? "0" : "") + date.getMinutes()}`
                        $object.find("td[row_data=date]").find("p").text(date_string)
                        $object.find("td[row_data=date]").attr("sorttable_customkey", `${date.getFullYear()}${(date.getMonth() < 10 ? "0" : "") + date.getMonth()}${(date.getDate() < 10 ? "0" : "") + date.getDate()}${(date.getHours() < 10 ? "0" : "") + date.getHours()}${(date.getMinutes() < 10 ? "0" : "") + date.getMinutes()}${(date.getSeconds() < 10 ? "0" : "") + date.getSeconds()}`)

                        $object.find("td[row_data=origin]").find("p").text(data.origin)
                        $object.find("td[row_data=destination]").find("p").text(data.destination)
                        $object.find("td[row_data=reason]").find("p").text(data.reason)

                        var is_approved = data.is_approved
                        if (is_approved == undefined) {
                            // pass is pending approval
                            $object.find("td[row_data=approved]").find("span").addClass("fa-clock")
                            $object.find("td[row_data=approved]").attr("sorttable_customkey",0)
                        } else if (is_approved == false) {
                            // pass was denied
                            $object.find("td[row_data=approved]").find("span").addClass("fa-times")
                            $object.find("td[row_data=approved]").attr("sorttable_customkey",-1)
                        } else {
                            // pass was approved
                            $object.find("td[row_data=approved]").find("span").addClass("fa-check ")
                            $object.find("td[row_data=approved]").attr("sorttable_customkey",1)
                        }

                        $t_body.append($object)
                    })
                })
            },
            (error) => {
                console.error(error)
            }
        )
})
