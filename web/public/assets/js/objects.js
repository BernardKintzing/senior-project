var async_return = {
    success: undefined,
    return: null,
    error: null
};

var account_data = {
    uid: undefined,
    first_name: undefined,
    last_name: undefined,
    preferred_name: undefined,
    email: undefined,
    linked: false
};