//
//  FirebaseManager.swift
//  Hall Pass
//
//  Created by Bernard Kintzing on 4/8/21.
//

import Foundation
import Firebase

class FirebaseManager {
    
    let auth: AuthManager
    let firestore: FirestoreManager
    let functions: FunctionsManager
    
    init() {
    
        auth = AuthManager()
        firestore = FirestoreManager()
        functions = FunctionsManager()
        
        NotificationCenter.default.addObserver(self, selector: #selector(authDidChange(_:)), name: Notification.Name.AuthStateDidChange, object: nil)
    }
    
    @IBAction func authDidChange(_ notification: Notification) {
        if let uid = auth.authUser?.uid {
            firestore.attachAccountListener(uid)
        } else {
            firestore.detachAccountListener()
        }
    }
}
