//
//  Firestore.swift
//  Hall Pass
//
//  Created by Bernard Kintzing on 4/8/21.
//

import Foundation
import Firebase

var currentUser: HPCurrentUser?

// depending on the account type the values will be empty
var teachers: [HPUser] = []
var students: [HPUser] = []

class FirestoreManager {
    
    let database = Firestore.firestore()
    var accountListener: ListenerRegistration?
    
    // MARK: - Account Functions
    func attachAccountListener(_ uid: String) {
        
        accountListener = database.collection("accounts").document(uid).addSnapshotListener { documentSnapshot, error in
            guard let document = documentSnapshot else {
                return
            }
            
            if let data = document.data() {
                
                let accountType = data["account_type"] as? String ?? ""
                let school = data["school"] as? String ?? ""
                
                currentUser = HPCurrentUser(uid: uid,
                                            firstName: data["first_name"] as? String ?? "",
                                            lastName: data["last_name"] as? String ?? "",
                                            preferredName: data["preferred_name"] as? String ?? "",
                                            email: data["email"] as? String ?? "",
                                            accountType: accountType,
                                            school: school)
                
                if accountType == "teacher" {
                    
                } else if accountType == "student" {
                    self.getTeachers(school)
                }
                
                
                NotificationCenter.default.post(name: Notification.Name.AcccountDidChange, object: nil)
            }
        }
    }
    
    func detachAccountListener() {
        currentUser = nil
        teachers = []
        students = []
        
        if let accountListener = accountListener {
            accountListener.remove()
        }
    }
    
    func createAccount(account: User?, accountType: String, completionHandler: @escaping (_ success: Bool, _ error: Error?) -> Void) {
        
        guard let account = account,
              let email = account.email
        else {
            completionHandler(false, HPError.accountReturnError)
            return
        }
        
        database.collection("accounts")
            .document(account.uid)
            .setData(["email": email,
                      "account_type": accountType,])
            { error in
                if let error = error {
                    completionHandler(false, error)
                } else {
                    completionHandler(true, nil)
                }
            }
    }
    
    func updateAccount(completionHandler:@escaping(Bool)->()) {
        
        guard let uid = currentUser?.uid else {
            completionHandler(false)
            return
        }
        
        let firstName = currentUser?.firstName ?? ""
        let lastName = currentUser?.lastName ?? ""
        let preferredName = currentUser?.preferredName ?? ""
        let school = currentUser?.school ?? ""
        
        database.collection("accounts")
            .document(uid)
            .updateData(["first_name": firstName,
                         "last_name": lastName,
                         "preferred_name": preferredName,
                         "school": school])
            { error in
                if let _ = error {
                    completionHandler(false)
                } else {
                    completionHandler(true)
                }
            }
    }
    
    // MARK: - Hall Pass Functions
    func getStudentHallPasses(completionHandler:@escaping([HallPass])->())  {
        
        guard let uid = currentUser?.uid else {
            completionHandler([])
            return
        }
        
        database.collection("passes").whereField("student_id", isEqualTo: uid).order(by: "date", descending: true).getDocuments() { (querySnapshot, error) in
            completionHandler(self.processHallPassQuery(querySnapshot))
        }
    }
    
    func getTecherHallPasses(completionHandler:@escaping([HallPass])->())  {
        
        guard let uid = currentUser?.uid else {
            completionHandler([])
            return
        }
        
        database.collection("passes").whereField("teachers", arrayContains: uid).order(by: "date", descending: true).getDocuments() { (querySnapshot, error) in
            completionHandler(self.processHallPassQuery(querySnapshot))
        }
    }
    
    func getStaffHallPasses(completionHandler:@escaping([HallPass])->())  {
        
        guard let school = currentUser?.school else {
            completionHandler([])
            return
        }
        
        
        database.collection("passes").whereField("school", isEqualTo: school).order(by: "date", descending: true).getDocuments() { (querySnapshot, error) in
                completionHandler(self.processHallPassQuery(querySnapshot))
            }
        }
        
        func processHallPassQuery(_ querySnapshot: QuerySnapshot?) -> [HallPass] {
            
            guard let querySnapshot = querySnapshot else { return [] }
            
            var hallPasses: [HallPass] = []
            
            for document in querySnapshot.documents {
                let data = document.data()
                
                if let date = (data["date"] as? Timestamp)?.dateValue(),
                   let origin = data["origin"] as? String,
                   let destination = data["destination"] as? String,
                   let reason = data["reason"] as? String,
                   let school = data["school"] as? String,
                   let studentID = data["student_id"] as? String,
                   let teachers = data["teachers"] as? [String] {
                    
                    hallPasses.append(HallPass(id: document.documentID,
                                               date: date.convertToTimeZone(initTimeZone: TimeZone(identifier: "UTC")!, timeZone: TimeZone.current),
                                               origin: origin,
                                               destination: destination,
                                               isApproved: data["is_approved"] as? Bool ?? false,
                                               reason: reason,
                                               school: school,
                                               studentID: studentID,
                                               teachers: teachers))
                    
                }
            }
            
            return hallPasses
        }
        
        func approvePass(id: String, isApproved: Bool) {
            database.collection("passes").document(id).updateData([
                "is_approved": isApproved
            ]) { error in
                if let error = error {
                    print("Error updating document: \(error)")
                } else {
                    NotificationCenter.default.post(name: Notification.Name.HallPassDidUpdate, object: nil)
                }
            }
        }
        
        // MARK: - School Functions
        func getSchools(completionHandler:@escaping([School])->()) {
            database.collection("schools").getDocuments() { (querySnapshot, error) in
                
                if let error = error {
                    print("Error getting documents: \(error)")
                    completionHandler([])
                } else {
                    var schools: [School] = []
                    
                    for document in querySnapshot!.documents {
                        let data = document.data()
                        
                        schools.append(School(id: document.documentID,
                                              name: data["name"] as? String ?? "",
                                              primaryEmail: data["primary_email"] as? String ?? "",
                                              primaryPhone: data["primary_phone"] as? String ?? "",
                                              addressLineOne: data["address_line_one"] as? String ?? "",
                                              addressLineTwo: data["address_line_two"] as? String ?? "",
                                              city: data["city"] as? String ?? "",
                                              state: data["state"] as? String ?? "",
                                              postalCode: data["postal_code"] as? String ?? "",
                                              country: data["country"] as? String ?? ""))
                    }
                    
                    completionHandler(schools)
                }
            }
        }
        
        func getTeachers(_ school_id: String) {
            
            database.collection("accounts")
                .whereField("school", isEqualTo: school_id)
                .whereField("account_type", isEqualTo: "teacher")
                .getDocuments { (querySnapshot, error) in
                    
                    if let error = error {
                        print("Error getting documents: \(error)")
                    } else {
                        
                        teachers = []
                        
                        for document in querySnapshot!.documents {
                            let data = document.data()
                            
                            teachers.append(HPUser(uid: document.documentID,
                                                   firstName: data["first_name"] as? String ?? "",
                                                   lastName: data["last_name"] as? String ?? "",
                                                   preferredName: data["preferred_name"] as? String ?? "",
                                                   email: data["email"] as? String ?? "",
                                                   school: data["school"] as? String ?? ""))
                        }
                        
                        
                        NotificationCenter.default.post(name: Notification.Name.TeachersDidUpdate, object: nil)
                    }
                }
        }
    }
