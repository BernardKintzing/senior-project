//
//  Auth.swift
//  Hall Pass
//
//  Created by Bernard Kintzing on 4/8/21.
//

import Foundation
import Firebase

class AuthManager {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    private let auth = Auth.auth()
    private var handle: AuthStateDidChangeListenerHandle?
    var authUser: User?
    
    init() {
        handle = auth.addStateDidChangeListener { [self] (auth, user) in
            
            authUser = user
            NotificationCenter.default.post(name: Notification.Name.AuthStateDidChange, object: nil)
        }
    }
    
    func signIn(email: String, password: String, completionHandler: @escaping (_ success: Bool, _ error: Error?) -> Void) {
        auth.signIn(withEmail: email, password: password) { (result, error) in
            if let error = error, let _ = AuthErrorCode(rawValue: error._code) {
                completionHandler(false, error)
            } else {
                completionHandler(true, nil)
            }
        }
    }
    
    func signOut(completionHandler: @escaping (_ success: Bool) -> Void) {
        do {
            try auth.signOut()
            completionHandler(true)
        } catch {
            completionHandler(false)
        }
    }
    
    func createAccount(email: String, password: String, accountType: String, completionHandler: @escaping (_ success: Bool, _ error: Error?) -> Void) {
        auth.createUser(withEmail: email, password: password) { (result, error) in
            if let error = error, let _ = AuthErrorCode(rawValue: error._code) {
                completionHandler(false, error)
            } else {
                self.appDelegate.firebase?.firestore.createAccount(account: result?.user, accountType: accountType, completionHandler: { (success, error) in
                    if let error = error, let _ = AuthErrorCode(rawValue: error._code) {
                        completionHandler(false, error)
                    } else {
                        completionHandler(true, nil)
                    }
                })
            }
        }
    }
}


