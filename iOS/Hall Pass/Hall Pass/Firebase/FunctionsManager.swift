//
//  FunctionsManager.swift
//  Hall Pass
//
//  Created by Bernard Kintzing on 4/11/21.
//

import Foundation
import Firebase

class FunctionsManager {
    let functions = Functions.functions()
    
    
    func requestHallPass(_ hallPass: HallPass, completionHandler:@escaping(Bool)->()) {
        
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy h:mm a"
        let stringDate = dateFormatter.string(from: hallPass.date)
        
        functions.httpsCallable("student-request_pass").call(["student_id": hallPass.studentID,
                                                              "date": stringDate,
                                                              "origin": hallPass.origin,
                                                              "destination": hallPass.destination,
                                                              "teachers": hallPass.teachers,
                                                              "reason": hallPass.reason,
                                                              "school": hallPass.school])
        { (result, error) in
            if let error = error as NSError? {
                print(error.localizedDescription)
            } else if let data = result?.data as? [String: Any],
                      let success = data["success"] as? Bool {
                
                if success {
                    completionHandler(true)
                    return
                } else if let error = data["error"] as? String {
                    print(error)
                }
            }
            
            completionHandler(false)
        }
    }
    
    func getName(_ uid: String, completionHandler:@escaping(String?)->()) {
        functions.httpsCallable("auth-name_for_uid").call(["uid": uid])
        { (result, error) in
            if let error = error as NSError? {
                print(error.localizedDescription)
            } else if let data = result?.data as? [String: Any],
                      let success = data["success"] as? Bool {
                
                if success {
                    if let name = data["return"] as? String {
                    completionHandler(name)
                        return
                    }
                } else if let error = data["error"] as? String {
                    print(error)
                }
            }
            
            completionHandler(nil)
        }
    }
}
