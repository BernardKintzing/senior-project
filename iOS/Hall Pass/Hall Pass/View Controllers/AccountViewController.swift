//
//  AccountViewController.swift
//  Hall Pass
//
//  Created by Bernard Kintzing on 4/11/21.
//

import UIKit

class AccountViewController: UIViewController {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    var schools: [School] = []
    
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var preferredNameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var schoolPicker: UIPickerView!
    @IBOutlet weak var updateAccountButton: UIButton!
    @IBOutlet weak var signOutButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Hide keyboard when user taps
        self.hideKeyboardWhenTappedAround()
        
        // Hide navigation bar
        self.navigationController?.isNavigationBarHidden = true
        
        updateAccountButton.filledPrimary()
        updateAccountButton.large()
        signOutButton.filledSecondary()
        signOutButton.large()
        
        schoolPicker.delegate = self
        schoolPicker.dataSource = self
        
        emailTextField.isEnabled = false
        
        if let currrentUser = currentUser {
            firstNameTextField.text = currrentUser.firstName
            lastNameTextField.text = currrentUser.lastName
            preferredNameTextField.text = currrentUser.preferredName
            emailTextField.text = currrentUser.email
        }
        
        appDelegate.firebase?.firestore.getSchools { schools in
            self.schools = schools
            self.schoolPicker.reloadComponent(0)
        }
    }
    
    @IBAction func didSelectUpdateAccount(_ sender: Any) {
        guard let firstName = firstNameTextField.text else {
            errorLabel.text = "Error: First name is required."
            errorLabel.isHidden = false
            return
        }
        guard let lastName = lastNameTextField.text else {
            errorLabel.text = "Error: Last name is required."
            errorLabel.isHidden = false
            return
        }
        let preferredName = preferredNameTextField.text ?? ""
        let selectedIndex = schoolPicker.selectedRow(inComponent: 0)
        var school = ""
        if selectedIndex != 0 {
            school = schools[selectedIndex - 1].id
        }
        
        // validate form
        if firstName == "" {
            errorLabel.text = "Error: First name is required."
            errorLabel.isHidden = false
            return
        }
        
        if lastName == "" {
            errorLabel.text = "Error: Last name is required."
            errorLabel.isHidden = false
            return
        }
        
        // update current user
        currentUser?.firstName = firstName
        currentUser?.lastName = lastName
        currentUser?.preferredName = preferredName
        currentUser?.school = school
        
        appDelegate.firebase?.firestore.updateAccount { success in
            if success {
                let alert = UIAlertController(title: "Success", message: "Account succesfully updated.", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default))
                self.present(alert, animated: true, completion: nil)
            } else {
                let alert = UIAlertController(title: "Error", message: "An unknown error occured trying to update profile.", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func didSelectSignOut(_ sender: Any) {
        appDelegate.firebase?.auth.signOut() { success in
            if !success {
                let alert = UIAlertController(title: "Error", message: "An unknown error occured.", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
}

extension AccountViewController: UIPickerViewDelegate {
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 25
    }
    
}

extension AccountViewController: UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return schools.count + 1
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        
        var pickerLabel = view as? UILabel;

        if (pickerLabel == nil)
        {
            pickerLabel = UILabel()

            pickerLabel?.font = UIFont(name: "System", size: 14)
            pickerLabel?.textAlignment = NSTextAlignment.center
        }
        
        if row == 0 {
            pickerLabel?.text = "----- No School ------";
        } else {
            pickerLabel?.text = schools[row - 1].name;
            
            if currentUser?.school == schools[row - 1].id {
                pickerView.selectRow(row, inComponent: 0, animated: false)
            }
        }

        return pickerLabel!;
    }
}


