//
//  PassViewController.swift
//  Hall Pass
//
//  Created by Bernard Kintzing on 4/11/21.
//

import UIKit

class PassViewController: UIViewController {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var studentLabel: UILabel!
    @IBOutlet weak var teacherTitleLabel: UILabel!
    @IBOutlet weak var teacherLabel: UILabel!
    @IBOutlet weak var originLabel: UILabel!
    @IBOutlet weak var destinationLabel: UILabel!
    @IBOutlet weak var approvedSwitch: UISwitch!
    @IBOutlet weak var reasonLabel: UILabel!
    let dateFormatter = DateFormatter()
    
    var hallPass: HallPass? {
        didSet {
            if(dateLabel != nil && originLabel != nil && destinationLabel != nil && approvedSwitch != nil && reasonLabel != nil) {
                refresh()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Hide keyboard when user taps
        self.hideKeyboardWhenTappedAround()
        
        // Hide navigation bar
        self.navigationController?.isNavigationBarHidden = true
        
        dateFormatter.dateFormat = "EEEE, MMMM dd, yyyy, h:mm a"
        
        if let currentUser = currentUser {
            if(currentUser.accountType == "student") {
                approvedSwitch.isEnabled = false
            }
        }
        
        refresh()
    }
    
    func refresh() {
        guard let hallPass = hallPass else { return }
        
        dateLabel.text = dateFormatter.string(from: hallPass.date.convertToTimeZone(initTimeZone: TimeZone(identifier: "UTC")!, timeZone: TimeZone.current))
        originLabel.text = hallPass.origin
        destinationLabel.text = hallPass.destination
        
        approvedSwitch.isOn = hallPass.isApproved
        
        appDelegate.firebase?.functions.getName(hallPass.studentID) { name in
            guard let name = name else { return }
            self.studentLabel.text = name
        }
        
        
        if hallPass.teachers.count > 1 {
            teacherTitleLabel.text = "Teachers"
        } else {
            teacherTitleLabel.text = "Teacher"
        }
        
        var teacherText = ""
        var teacherCount = 0
        for teacher in hallPass.teachers {
            appDelegate.firebase?.functions.getName(teacher) { name in
                
                if let name = name {
                    
                    if teacherText != "" {
                        teacherText += ", "
                    }
                    
                    teacherText += name
                }
                
                teacherCount += 1
                
                if teacherCount == hallPass.teachers.count {
                    self.teacherLabel.text = teacherText
                }
            }
        }
        
        reasonLabel.text = hallPass.reason
    }
    
    @IBAction func didChangeApproved(_ sender: Any) {
        guard let id = hallPass?.id else { return }
        appDelegate.firebase?.firestore.approvePass(id: id, isApproved: approvedSwitch.isOn)
    }
}
