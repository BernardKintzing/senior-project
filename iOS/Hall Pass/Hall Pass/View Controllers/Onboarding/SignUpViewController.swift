//
//  SignUpViewController.swift
//  Hall Pass
//
//  Created by Bernard Kintzing on 4/14/21.
//

import UIKit

class SignUpViewController: UIViewController {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var confirmPasswordField: UITextField!
    @IBOutlet weak var accountTypePickerView: UIPickerView!
    @IBOutlet weak var createAccountButton: UIButton!
    @IBOutlet weak var signInButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        createAccountButton.filledPrimary()
        createAccountButton.large()
        signInButton.filledSecondary()
        signInButton.large()
        
        accountTypePickerView.delegate = self
        accountTypePickerView.dataSource = self
    }
    
    @IBAction func didSelectCreateAccount(_ sender: Any) {
        
        guard let email = emailField.text,
              let password = passwordField.text,
              let confirmPassword = confirmPasswordField.text,
              let accountType = accountTypes(rawValue: accountTypePickerView.selectedRow(inComponent: 0))?.description
        else { return }
        
        if password != confirmPassword {
            errorLabel.text = "Error: Passwords do not match."
        } else {
            appDelegate.firebase?.auth.createAccount(email: email, password: password, accountType: accountType, completionHandler:  {[weak self] (success, error) in
                guard let `self` = self else { return }
                
                if (!success) {
                    if let error = error {
                        self.errorLabel.text = error.localizedDescription
                        self.errorLabel.isHidden = false
                    }
                }
            })
        }
    }
    
    @IBAction func didSelectSignIn(_ sender: Any) {
        let viewController = SignInViewController(nibName: "SignInViewController", bundle: nil)
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}

extension SignUpViewController: UIPickerViewDelegate {
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 25
    }
    
}

extension SignUpViewController: UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return accountTypes.count
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        
        var pickerLabel = view as? UILabel;

        if (pickerLabel == nil)
        {
            pickerLabel = UILabel()

            pickerLabel?.font = UIFont(name: "System", size: 14)
            pickerLabel?.textAlignment = NSTextAlignment.center
        }

        pickerLabel?.text = accountTypes(rawValue: row)?.description;

        return pickerLabel!;
    }
}

