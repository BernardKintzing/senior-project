//
//  SignIn.swift
//  Hall Pass
//
//  Created by Bernard Kintzing on 4/8/21.
//

import UIKit

class SignInViewController: UIViewController {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var signInButton: UIButton!
    @IBOutlet weak var createAccountButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Hide keyboard when user taps
        self.hideKeyboardWhenTappedAround()
        
        signInButton.filledPrimary()
        signInButton.large()
        
        createAccountButton.filledSecondary()
        createAccountButton.large()
        
        // Hide navigation bar
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        NotificationCenter.default.removeObserver(self, name: Notification.Name.AcccountDidChange, object: nil)
    }
    
    @IBAction func didSelectSignIn(_ sender: Any) {
        
        guard let email = emailField.text,
              let password = passwordField.text
        else { return }
        
        appDelegate.firebase?.auth.signIn(email: email, password: password, completionHandler:  {[weak self] (success, error) in
            guard let `self` = self else { return }
            
            if (!success) {
                if let error = error {
                    self.errorLabel.text = error.localizedDescription
                    self.errorLabel.isHidden = false
                }
            }
        })
    }
    
    @IBAction func didSelectCreateAccount(_ sender: Any) {
        let viewController = SignUpViewController(nibName: "SignUpViewController", bundle: nil)
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}
