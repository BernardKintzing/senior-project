//
//  RedirectViewController.swift
//  Hall Pass
//
//  Created by Bernard Kintzing on 4/14/21.
//

import UIKit

class RedirectViewController: UIViewController {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        redirectUser()
        
        NotificationCenter.default.addObserver(self, selector: #selector(userUpdated(_:)), name: Notification.Name.AuthStateDidChange, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(userUpdated(_:)), name: Notification.Name.AcccountDidChange, object: nil)
    }
    
    @IBAction func userUpdated(_ notification: Notification) {
        redirectUser()
    }
    
    func redirectUser() {
        
        if let auth = appDelegate.firebase?.auth.authUser {
            // auth user is signed in
            
            
            if let user = currentUser {
                // profile retrieved from firestore
                
                switch user.accountType {
                case "student":
                    let viewController = StudentViewController(nibName: "StudentViewController", bundle: nil)
                    appDelegate.window?.rootViewController = UINavigationController(rootViewController: viewController)
                case "teacher":
                    let viewController = TeacherViewController(nibName: "TeacherViewController", bundle: nil)
                    appDelegate.window?.rootViewController = UINavigationController(rootViewController: viewController)
                case "staff":
                    let viewController = StaffViewController(nibName: "StaffViewController", bundle: nil)
                    appDelegate.window?.rootViewController = UINavigationController(rootViewController: viewController)
                default:
                    let viewController = SignInViewController(nibName: "SignInViewController", bundle: nil)
                    appDelegate.window?.rootViewController = UINavigationController(rootViewController: viewController)
                }
                
            } else {
                // request users profile
                appDelegate.firebase?.firestore.attachAccountListener(auth.uid)
            }
            
        } else {
            let viewController = SignInViewController(nibName: "SignInViewController", bundle: nil)
            appDelegate.window?.rootViewController = UINavigationController(rootViewController: viewController)
        }
    }
}
