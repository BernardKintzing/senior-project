//
//  HallPassCell.swift
//  Hall Pass
//
//  Created by Bernard Kintzing on 4/10/21.
//

import UIKit

class HallPassCell: UITableViewCell {
    
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var destinationLabel: UILabel!
    @IBOutlet weak var isApprovedImageView: UIImageView!
    
    var hallPass: HallPass? {
        didSet {
            
            guard let hallPass = hallPass else { return }
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MMM d, h:mm"
            timeLabel.text = dateFormatter.string(from: hallPass.date.convertToTimeZone(initTimeZone: TimeZone(identifier: "UTC")!, timeZone: TimeZone.current))
            
            destinationLabel.text = hallPass.destination
            
            if hallPass.isApproved {
                isApprovedImageView.image =  UIImage(systemName: "checkmark.circle")
            } else {
                isApprovedImageView.image =  UIImage(systemName: "xmark.circle")
            }
            
        }
    }
}
