//
//  RequestPassViewController.swift
//  Hall Pass
//
//  Created by Bernard Kintzing on 4/10/21.
//

import UIKit

class RequestPassViewController: UIViewController {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var originField: UITextField!
    @IBOutlet weak var destinationField: UITextField!
    @IBOutlet weak var reasonField: UITextField!
    @IBOutlet weak var teacherPicker: UIPickerView!
    @IBOutlet weak var teacherLabel: UILabel!
    @IBOutlet weak var requestPassButton: UIButton!
    
    var selectedTeachers: [HPUser] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Hide keyboard when user taps
        self.hideKeyboardWhenTappedAround()
        
        // Hide navigation bar
        self.navigationController?.isNavigationBarHidden = true
        
        requestPassButton.filledPrimary()
        requestPassButton.large()
        
        teacherLabel.text = ""
        
        teacherPicker.delegate = self
        teacherPicker.dataSource = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(teachersDidUpdate(_:)), name: Notification.Name.TeachersDidUpdate, object: nil)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(Notification.Name.TeachersDidUpdate)
    }
    
    @IBAction func teachersDidUpdate(_ notification: Notification) {
        teacherPicker.reloadAllComponents()
    }
    
    @IBAction func didSelectAddTeacher(_ sender: Any) {
        
        let teacher = teachers[teacherPicker.selectedRow(inComponent: 0)]
        
        if !selectedTeachers.contains(teacher) {
            selectedTeachers.append(teacher)
            
            var name = "Unknown Name"
            if teacher.preferredName != "" {
                name = teacher.preferredName
            } else if teacher.firstName != "" {
                name = teacher.firstName
            }
            
            if let teacherText = teacherLabel.text {
                if teacherText == "" {
                    teacherLabel.text = name
                } else  {
                    teacherLabel.text = "\(teacherText), \(name)"
                }
                
            }
        }
    }
    
    @IBAction func didSelectRequestPass(_ sender: Any) {
        
        var isValid = true
        
        let date = datePicker.date
        
        let origin = originField.text ?? ""
        if origin == "" {
            isValid = false
            originField.layer.borderWidth = 1.0
            originField.layer.borderColor = UIColor.red.cgColor
        }
        
        let destination = destinationField.text ?? ""
        if destination == "" {
            isValid = false
            destinationField.layer.borderWidth = 1.0
            destinationField.layer.borderColor = UIColor.red.cgColor
        }
        
        let reason = reasonField.text ?? ""
        if reason == "" {
            isValid = false
            reasonField.layer.borderWidth = 1.0
            reasonField.layer.borderColor = UIColor.red.cgColor
        }
        
        var teachers: [String] = []
        for teacher in selectedTeachers {
            teachers.append(teacher.uid)
        }
        
        if isValid {
            
            appDelegate.firebase?.functions.requestHallPass(HallPass(id: "",
                                                                     date: date,
                                                                     origin: origin,
                                                                     destination: destination,
                                                                     isApproved: false,
                                                                     reason: reason,
                                                                     school: currentUser?.school ?? "",
                                                                     studentID: currentUser?.uid ?? "",
                                                                     teachers: teachers)) { success in
                if success {
                    let alert = UIAlertController(title: "Success", message: "Hall pass succesfully requested.", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default))
                    self.present(alert, animated: true, completion: nil)
                } else {
                    let alert = UIAlertController(title: "Error", message: "An unknown error occured.", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default))
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
    }
}

extension RequestPassViewController: UIPickerViewDelegate {
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 50
    }
    
}

extension RequestPassViewController: UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return teachers.count
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let view: TeacherRowView = .fromNib()
        let teacher = teachers[row]
        
        if teacher.preferredName != "" {
            view.nameLabel.text = teacher.preferredName
        } else if teacher.firstName != "" {
            view.nameLabel.text = teacher.firstName
        }
        
        view.emailLabel.text = teacher.email
        
        view.frame.size.height = 50
        view.frame.size.width = self.view.frame.width - 60
        
        return view
    }
}
