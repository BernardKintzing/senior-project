//
//  StudentViewController.swift
//  Hall Pass
//
//  Created by Bernard Kintzing on 4/8/21.
//

import UIKit

class StudentViewController: UIViewController {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    var hallPasses: [HallPass] = []
    
    @IBOutlet weak var profileTitle: UILabel!
    @IBOutlet weak var hallPassesTableView: UITableView!
    @IBOutlet weak var accountButton: UIButton!
    @IBOutlet weak var requestPassButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Hide keyboard when user taps
        self.hideKeyboardWhenTappedAround()
        
        // Hide navigation bar
        self.navigationController?.isNavigationBarHidden = true
        
        
        accountButton.filledPrimary()
        accountButton.small()
        requestPassButton.filledSecondary()
        requestPassButton.small()
        
        // Table View Setup
        self.hallPassesTableView.delegate = self;
        self.hallPassesTableView.dataSource = self;
        self.hallPassesTableView.register(UINib(nibName: "HallPassCell", bundle: nil), forCellReuseIdentifier: "HallPassCell")
        
        appDelegate.firebase?.firestore.getStudentHallPasses { hallPasses in
            self.hallPasses = hallPasses
            self.hallPassesTableView.reloadData()
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(accountDidChange(_:)), name: Notification.Name.AcccountDidChange, object: nil)
        
        refresh()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        NotificationCenter.default.removeObserver(self, name: Notification.Name.AcccountDidChange, object: nil)
    }
    
    @IBAction func didSelectAccount(_ sender: Any) {
        let viewController = AccountViewController(nibName: "AccountViewController", bundle: nil)
        present(viewController, animated: true)
    }
    
    @IBAction func didSelectRequestPass(_ sender: Any) {
        let viewController = RequestPassViewController(nibName: "RequestPassViewController", bundle: nil)
        present(viewController, animated: true)
    }
    
    @IBAction func accountDidChange(_ notification: Notification) {
        refresh()
    }
    
    func refresh() {
        if let currentUser = currentUser {
            
            if currentUser.preferredName != "" {
                profileTitle.text = "Welcome, \(currentUser.preferredName)"
            } else if currentUser.firstName != "" {
                profileTitle.text = "Welcome, \(currentUser.firstName)"
            }
        }
    }
}

// MARK: - UITableViewDataSource
extension StudentViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return hallPasses.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "HallPassCell", for: indexPath) as! HallPassCell
        cell.frame.size.width = self.view.frame.width
        cell.hallPass = hallPasses[indexPath.row]
        
        return cell
    }
    
}

// MARK: - UITableViewDelegate
extension StudentViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath)! as? HallPassCell {
            cell.isSelected = false
            
            
            let viewController = PassViewController(nibName: "PassViewController", bundle: nil)
            viewController.hallPass = hallPasses[indexPath.row]
            present(viewController, animated: true)
        }
    }
}
