//
//  TeacherRowView.swift
//  Hall Pass
//
//  Created by Bernard Kintzing on 4/11/21.
//

import UIKit

class TeacherRowView: UIView {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
}
