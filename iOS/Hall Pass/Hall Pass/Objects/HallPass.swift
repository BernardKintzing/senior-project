//
//  HallPass.swift
//  Hall Pass
//
//  Created by Bernard Kintzing on 4/10/21.
//

import Foundation

struct HallPass {
    var id: String
    var date: Date
    var origin: String
    var destination: String
    var isApproved: Bool
    var reason: String
    var school: String
    var studentID: String
    var teachers: [String]    
}
