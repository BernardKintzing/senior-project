//
//  CurrentUser.swift
//  Hall Pass
//
//  Created by Bernard Kintzing on 4/8/21.
//

import Foundation

struct HPUser: Equatable {
    var uid: String
    var firstName: String
    var lastName: String
    var preferredName: String
    var email: String
    var school: String
    var linked: Bool = false
}

struct HPCurrentUser {
    var uid: String
    var firstName: String
    var lastName: String
    var preferredName: String
    var email: String
    var accountType: String
    var school: String
}

public enum accountTypes: Int {
    case student = 0
    case teacher = 1
    case staff = 2
    static var count: Int { return 3 }
    
    var description: String {
        switch self {
        case .student: return "student"
        case .teacher: return "teacher"
        case .staff: return "staff"
        }
    }
}
