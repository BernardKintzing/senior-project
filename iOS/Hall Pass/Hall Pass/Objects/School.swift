//
//  School.swift
//  Hall Pass
//
//  Created by Bernard Kintzing on 4/14/21.
//

import Foundation

struct School {
    var id: String
    var name: String
    var primaryEmail: String
    var primaryPhone: String
    var addressLineOne: String
    var addressLineTwo: String
    var city: String
    var state: String
    var postalCode: String
    var country: String
}
