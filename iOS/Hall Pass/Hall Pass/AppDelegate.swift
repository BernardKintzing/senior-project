//
//  AppDelegate.swift
//  Hall Pass
//
//  Created by Bernard Kintzing on 4/8/21.
//

import UIKit
import Firebase

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    var firebase: FirebaseManager?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        // Initialize Firebase
        FirebaseApp.configure()
        firebase = FirebaseManager()
        
        // redirect user based on signed in status
        window = UIWindow(frame: UIScreen.main.bounds)
        redirect()
        
        NotificationCenter.default.addObserver(self, selector: #selector(authDidChange(_:)), name: Notification.Name.AuthStateDidChange, object: nil)
        
        
        return true
    }
    
    
    @IBAction func authDidChange(_ notification: Notification) {
        redirect()
    }
    
    func redirect() {
        window?.rootViewController = UINavigationController(rootViewController: RedirectViewController(nibName: "RedirectViewController", bundle: nil))
        window?.makeKeyAndVisible()
    }
}

