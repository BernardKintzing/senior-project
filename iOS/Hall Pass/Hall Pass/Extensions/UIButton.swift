//
//  UIButton.swift
//  Hall Pass
//
//  Created by Bernard Kintzing on 4/14/21.
//

import UIKit

extension UIButton {
    
    func filledPrimary() {
        self.setTitleColor(UIColor(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        self.backgroundColor = UIColor(red: 0.2, green: 0.69, blue: 0.97, alpha: 0.8)
        self.layer.borderColor = UIColor(red: 0.34, green: 0.69, blue: 0.93, alpha: 1.0).cgColor
        self.layer.borderWidth = 1.0
        self.layer.cornerRadius = 5.0
    }
    
    func filledSecondary() {
        self.setTitleColor(UIColor(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        self.backgroundColor = UIColor(red: 0.87, green: 0.87, blue: 0.87, alpha: 0.8)
        self.layer.borderColor = UIColor(red: 0.87, green: 0.87, blue: 0.87, alpha: 1.0).cgColor
        self.layer.borderWidth = 1.0
        self.layer.cornerRadius = 5.0
    }
    
    func large() {
        self.widthAnchor.constraint(equalToConstant: 250).isActive = true
        self.heightAnchor.constraint(equalToConstant: 35).isActive = true
    }
    
    func medium() {
        self.widthAnchor.constraint(equalToConstant: 200).isActive = true
        self.heightAnchor.constraint(equalToConstant: 35).isActive = true
    }
    
    func small() {
        self.widthAnchor.constraint(equalToConstant: 150).isActive = true
        self.heightAnchor.constraint(equalToConstant: 35).isActive = true
    }
}
