//
//  UIView.swift
//  Hall Pass
//
//  Created by Bernard Kintzing on 4/11/21.
//

import UIKit

extension UIView {
    class func fromNib<T: UIView>() -> T {
        return Bundle(for: T.self).loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
}
