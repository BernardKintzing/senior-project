//
//  NSNotification.swift
//  Hall Pass
//
//  Created by Bernard Kintzing on 4/8/21.
//

import Foundation

extension NSNotification.Name {
    
    // auth
    static let AuthStateDidChange = Notification.Name("AuthStateDidChange")
    
    // firestore
    static let AcccountDidChange = Notification.Name("AcccountDidChange")
    static let TeachersDidUpdate = Notification.Name("TeachersDidUpdate")
    static let HallPassDidUpdate = Notification.Name("HallPassDidUpdate")
}
