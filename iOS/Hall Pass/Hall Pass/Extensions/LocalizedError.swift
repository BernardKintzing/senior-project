//
//  LocalizedError.swift
//  Hall Pass
//
//  Created by Bernard Kintzing on 4/14/21.
//

import Foundation

public enum HPError: LocalizedError {
    case accountReturnError
}

extension HPError {
    public var errorDescription: String? {
        switch self {
        case .accountReturnError:
            return NSLocalizedString("Error: Auth account failed to return.", comment: "HPError")
        }
    }
}
